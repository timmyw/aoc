(* Day 3a *)

open Core

let input = "../input/test_03"

(*
467..114..
...*......
..35..633.
......#...
617*......
.....+.58.
..592.....
......755.
...$.*....
.664.598..
*)

let nth' ll i = match List.nth ll i with
    | Some n -> n
    | None   -> ""

let get_char row col lines =
  let max_cols = (String.length (Twutils.head lines)) in
  let max_rows = List.length lines - 1 in
  let row' = if row < 0 then 0 else if row > max_rows then max_rows else row in
  let col' = if col < 0 then 0 else if col > max_cols then max_cols else col in
  let row_ = List.nth lines row' in
  let row = match row_ with
  | Some r -> r
  | None   -> "" in
  String.get row col'

let check_and_extract row col lines =
  let c = get_char row col lines in
  if Twutils.is_digit c
  then 1
  else 0

(* let extract_number l c = *)
(*   let rec extract_number' l c bld = *)
(*   in *)
(*   extract_number' l c "" *)
(*
.....
.XXX.
.X$X.
.XXX.
*)
let find_surrounding_numbers row col rows max_col =
  let coords = [ (-1, -1); (-1, 0); (-1,1); (0, -1); (0, 1); (1, -1); (1, 0); (1,1)] in
  let m = List.map ~f:
            (fun (drow, dcol) ->
              if row + drow >= 0
                 && row + drow < List.length rows
                 && col + dcol >= 0
                 && col + dcol < max_col
              then
                let line = nth' rows (row+drow) in
                extract_number line (col+dcol)
              else
                0
            )
            coords in
  m

(*
   On current line
     Scan through to find a symbol
     When symbol find
       On previous line look above and to left and right
       On following line look below and to left and right
       On current line look left and right
*)
let scan_line (cur_row:int) (rows:string list) =
  let line = nth' rows cur_row in
  List.foldi ~init:0
    ~f:(fun cur_col acc c ->
      if not (Twutils.is_digit c) && not (phys_equal c '.') then
        (* Find any surrounding number *)
        acc + find_surrounding_numbers cur_row cur_col rows (String.length line)
      else
        acc
    )
    (Twutils.explode line)

let rec handle_lines (acc:int)
          (cur_row:int)
          (max_rows:int)
          (rows:string list) =
  if cur_row >= max_rows
  then
    acc
  else
    let acc' = acc + scan_line cur_row rows in
    handle_lines acc' (cur_row+1) max_rows rows

let () =
  let lines = In_channel.read_lines input in
  let m = handle_lines 0 0 (List.length lines) lines in
  printf "%d\n" m
