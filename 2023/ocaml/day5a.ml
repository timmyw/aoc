(* Day 5a *)

open Core

let input = "../input/05"

let get_input = if Array.length (Sys.get_argv ())  > 1
               then
                 let args = Sys.get_argv () in
                 args.(1)
                else input

(* seeds: 79 14 55 13 *)
let rec get_seeds ls =
  let extract_seeds s =
    List.map ~f:Int.of_string
      (String.split ~on:' ' (Twutils.get_element s ':' 1 "") |> List.filter ~f:Twutils.string_not_empty) in
  match ls with
  | l::ll -> if Twutils.string_contains l "seeds:" (* We may still
                                                      have a blank
                                                      line *)
             then
               let seeds = extract_seeds l in (seeds, ll)
             else
               get_seeds ll
  | _     -> ([], [])

type mapping =
  {
    span_start : int;
    span_end   : int;
    dest_start : int;
    offset     : int;
  }

let apply_to_one_seed (maps:mapping list) (s:int) =
  List.fold_left ~init:s
    ~f:(fun acc mapping ->
      let _ = printf "%d %d %d\n" acc mapping.span_start mapping.span_end in
      if acc == s && acc >= mapping.span_start && acc < mapping.span_end
      then
        let _ = printf "IN SPAN: %d -> %d\n" acc (acc+mapping.offset) in
        acc + mapping.offset
      else acc)
    maps

let dump_map (ms : mapping list) =
  let _ = printf "Map size: %d\n" (List.length ms) in
  List.map ~f:(fun m -> printf "MAP: %d %d %d %d\n" m.span_start m.span_end m.dest_start m.offset) ms

let apply_map (ms : mapping list) (ss : int list) =
  let _ = dump_map ms in
  List.map ~f:(fun s -> apply_to_one_seed ms s) ss (* For each seed *)

let extract_mapping l =
  let x = Int.of_string (Twutils.get_element l ' ' 0 "") in
  let z = Int.of_string (Twutils.get_element l ' ' 1 "") in
  let len = Int.of_string (Twutils.get_element l ' ' 2 "") in
  (* let _ = printf "%d %d %d\n" x (x+len) z in *)
  { span_start = z;
    span_end = z + len;
    dest_start = x;
    offset = x - z;
  }

(*
  start building a map (assumes they're in order - so don't care about
  names?)
  When have full map (blank line), translate current seeds
  When lines is empty, then return most recent seeds
*)
let rec run_maps lines seeds  map_bld =
  match lines with
  | [] -> if List.length map_bld > 0
          then apply_map map_bld seeds
          else seeds
  | l::ll -> if Twutils.string_empty l
             then               (* End current map *)
               if List.length map_bld > 0
               then             (* Apply current map *)
                 let seeds' = apply_map map_bld seeds in
                 run_maps ll seeds' [] (* Start a new map *)
               else             (* We don't have a map - move on *)
                 run_maps ll seeds map_bld
             else               (* Add to the current map *)
               let map_bld' =
                 if not (String.contains l ':')
                 then             (* We have a mapping line *)
                   map_bld @ [extract_mapping l]
                 else
                   map_bld in
               run_maps ll seeds map_bld'

let () =
  let lines = In_channel.read_lines get_input in
  let (seeds, lines') = get_seeds lines in
  (* let seeds' = [14] in *)
  let destinations = run_maps lines' seeds [] in
  let min_dest = List.fold_left ~init:9999999999 ~f:(fun acc v -> if v < acc then v else acc) destinations in
  printf "%d\n" min_dest
  (* let _ = List.map ~f:(fun s -> printf "%d\n" s) destinations in *)
