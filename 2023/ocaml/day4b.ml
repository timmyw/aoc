(* Day 4b *)

open Core

let input = "../input/test_04"

type card = { num: int; winning: int list; played: int list }

let blank_card = { num=(-1); winning=[]; played=[] }

let strip_option os =
  match os with
  | None   -> ""
  | Some s -> s

let split_nums n =
  let ns = String.split ~on:'|' n in
  (List.nth ns 0 |> strip_option, List.nth ns 1  |> strip_option)

let split_card_nums l =
  let ls = String.split ~on:':' l in
  (List.nth ls 0 |> strip_option, List.nth ls 1 |> strip_option)

let print_str_list ii =
  let _ = List.map ~f:(fun x -> Core.printf "[%s] " x) ii in
  print_endline ""

let not_empty s = (String.length s) > 0

let split_list s =
  let ss = String.split ~on:' ' (Twutils.strip s) in
  (* let _ = List.map *)
  (*           ~f:(fun x -> Core.printf "[%s] %b " x (not_empty x)) *)
  (*           ss in *)
  let filtered = List.filter ~f:(fun x -> not_empty x) ss in
  (* let _ = print_str_list filtered in *)
  List.map ~f:Int.of_string filtered

let rec parse_lines card_num lines cards =
  match lines with
  | []   -> cards
  | l::ll ->
      let (_, nums) = split_card_nums l in
      let (winnings, playeds) = split_nums nums in
      let (winning, played) = (split_list winnings, split_list playeds) in
      let cards' = { num = card_num; winning = winning; played = played } :: cards in
      parse_lines (card_num + 1) ll cards'

let find_card cards num =
  match List.find ~f:() cards with
  | None   -> blank_card
  | Some c -> c

(* let find_winning { num=num; winning=winning; played=played} cards = *)
(*   let sum = List.fold *)
(*               ~init:[] *)
(*               ~f:(fun acc i -> match List.find ~f:(fun j -> phys_equal j i) winning with *)
(*                  | None -> acc *)
(*                  | Some i -> *)

let () =
  let input_ = if Array.length (Sys.get_argv ())  > 1
               then (Sys.get_argv ()).(1)
               else input in
  let lines = In_channel.read_lines input_ in
  let cards = parse_lines 0 lines [] in
  (*
    check each card
    - for X winning numbers, add X additional copies of the next card
   *)
  (* let remaining = List.fold *)
  (* let m = handle_lines 0 lines in *)
  printf "%d\n" 0
