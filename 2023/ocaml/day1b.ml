(* Day 1 *)

open Core

let input = "../input/01"

let number_strings = [ "one"; "two"; "three"; "four"; "five"; "six"; "seven"; "eight"; "nine" ]

let match_number s =
  let i = List.foldi number_strings ~init:0 ~f:(fun idx acc ns ->
      if String.equal ns (Twutils.sub_str s ~pos:0 ~len:(String.length ns))
      then
        (* let _ = printf "MATCH: %s %s\n" ns s in *)
        idx+1
      else acc) in
  if i > 0 then
    i
  else
    let first = String.get s 0 in
    if Twutils.is_digit first then Stdlib.Char.code first - 48 else 0

let rec handle_line lo hi l =
  if String.equal l ""
  then (lo, hi)
  else
    let num = match_number l in
    let (lo', hi') = if num > 0 then
        if phys_equal lo 0 then (num, num) else (lo, num)
      else
        (lo, hi) in
    handle_line lo' hi' (Twutils.right l (String.length l - 1))

let rec handle_lines acc lines =
  match lines with
  | [] -> acc
  | l::ll ->
    let (lo, hi) = handle_line 0 0 l in
    let acc' = acc + (lo * 10 + hi) in
    handle_lines acc' ll

let () =
  let lines = In_channel.read_lines input in
  let m = handle_lines 0 lines in
  printf "%d\n" m
