(* Day 5a *)

open Core

let input = "../input/test_05"

let get_input = if Array.length (Sys.get_argv ())  > 1
               then
                 let args = Sys.get_argv () in
                 args.(1)
               else input

(* seeds: 79 14 55 13 *)
let extract_seeds l =
  let seed_list = Twutils.get_element l ':' 1 "" in
  List.map ~f:Int.of_string (String.split ~on:' ' seed_list |>
                               List.filter ~f:Twutils.string_not_empty)

let get_seeds (lines : string list) : int list * string list =
  match lines with
  | l :: ll -> let seeds = extract_seeds l in
               (seeds, ll)
  | _       -> ([], lines)

let extract_maps ls =
  let extract_maps' ls maps build =
    match ls with
    | []    -> maps
    | l::ll -> asd in
  extract_maps' ls [] []

let () =
  let lines = In_channel.read_lines get_input in
  let (seeds, lines') = get_seeds lines in
  let maps = extract_maps lines' in
  (* let destinations = map_destinations maps seeds in *)
  let _ = List.map ~f:(fun s -> printf "%d\n" s) seeds in


  (* printf "%d\n" ((List.nth destinations 3) |> Twutils.strip_option 0) *)
  (* match List.nth maps 0 |> Twutils.strip_option ("", []) with *)
  (* let _ = List.map ~f:dump_map maps in *)
  ()

(*
  - read list of seeds

  - Read next map <- assumes next map is the right one (i.e. maps in
  right sequence).

    - for each seed (or current destination) - find the next destination

    - Continue until no more maps

  - Find the lowest destination
*)
