(* Day 4a *)

open Core

let input = "../input/test_04"

let strip_option os =
  match os with
  | None   -> ""
  | Some s -> s

(*
 41 48 83 86 17 | 83 86  6 31 17  9 48 53
*)
let split_nums n =
  let ns = String.split ~on:'|' n in
  (List.nth ns 0 |> strip_option, List.nth ns 1  |> strip_option)

(*
Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53
*)
let split_card_nums l =
  let ls = String.split ~on:':' l in
  (List.nth ls 0 |> strip_option, List.nth ls 1 |> strip_option)

let print_str_list ii =
  let _ = List.map ~f:(fun x -> Core.printf "[%s] " x) ii in
  print_endline ""

let not_empty s = (String.length s) > 0

let split_list s =
  let ss = String.split ~on:' ' (Twutils.strip s) in
  let _ = print_str_list ss in
  (* let _ = List.map *)
  (*           ~f:(fun x -> Core.printf "[%s] %b " x (not_empty x)) *)
  (*           ss in *)
  let filtered = List.filter ~f:(fun x -> not_empty x) ss in
  (* let _ = print_str_list filtered in *)
  List.map ~f:Int.of_string filtered

let rec handle_lines (acc:int)
          (rows:string list) =
  match rows with
  | []    -> acc
  | l::ll ->
     let (_, nums) = split_card_nums l in
     let (winnings, playeds) = split_nums nums in
     let (winning, played) = (split_list winnings, split_list playeds) in
     let sum = List.fold
                 ~init:0
                 ~f:(fun acc i -> match List.find ~f:(fun j -> phys_equal j i) winning with
                 | None -> acc
                 | Some i -> match acc with
                               | 0 -> 1
                               | _ -> acc * 2)
                 played in
     handle_lines (acc+sum) ll

let () =
  let input_ = if Array.length (Sys.get_argv ())  > 1
               then Sys.argv.(1)
               else input in
  let lines = In_channel.read_lines input_ in
  let m = handle_lines 0 lines in
  printf "%d\n" m
