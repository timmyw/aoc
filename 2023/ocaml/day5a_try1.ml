(* Day 5a *)

open Core

let input = "../input/test_05"

let get_input = if Array.length (Sys.get_argv ())  > 1
               then Sys.argv.(1)
               else input

(* seeds: 79 14 55 13 *)
let extract_seeds l =
  let ls = String.split ~on:':' l in
  let seed_list = List.nth ls 1 |> Twutils.strip_option "" in
  List.map ~f:Int.of_string (String.split ~on:' ' seed_list |> List.filter ~f:(fun x -> if String.length x > 0 then true else false))

let get_seeds (lines : string list) : int list * string list =
  match lines with
  | l :: ll -> let seeds = extract_seeds l in
               (seeds, ll)
  | _       -> ([], lines)

(* 50 98 2 *)
let extract_line l =
  match String.split_on_chars ~on:[' '] l with
  | d::s::len::_ -> (Int.of_string d, Int.of_string s, Int.of_string len)
  | _ -> (-99, -99, 0)

(*
seed-to-soil map:
50 98 2
52 50 48
*)
type seed = int
type transform = (seed * seed * seed)
type transform_set = transform list

let extract_maps (lines : string list)
  : transform_set list =

  let rec extract_maps' (bld : transform_set)
      (maps : transform_set list)
      (lines : string list) : transform_set list =
    match lines with
    | []    -> bld :: maps
    | l::ll -> match String.length l with
              | 0 -> (* Blank line - check if we have a built map *)
                 extract_maps' [] (bld :: maps) ll
              | _ -> (* build *)
                let new_map = if String.contains l ':'
                  then bld
                  else (extract_line l) :: bld in
                extract_maps' new_map maps ll
                in
   extract_maps' [] [] lines

let apply_mapping (dest, src, len) (s : seed) =
  if s >= src && s <= src + len
    then
      dest + (s - src)
    else
      src

let run_one_seed (mappings : transform_set list) (s : seed) : seed =
  let rec run_one_seed' (mapping : transform_set) (s : seed) : seed =
    match mapping with
      | [] -> s
      | m::rest ->
        let s' = apply_mapping m s in
        run_one_seed' rest s' in
  run_one_seed'

let dump_map (name, mappings) =
  printf "%s\n" name;
  List.map ~f:(fun (dst, src, len) -> printf "%d %d %d\n" dst src len) mappings

let () =
  let input_ = get_input in
  let lines = In_channel.read_lines input_ in
  let (seeds, lines') = get_seeds lines in
  let maps = extract_maps lines' in
  (* For each seed
     Run through each map and apply to each seed *)
  let destinations = List.map
      ~f:(fun seed -> run_one_seed maps seed)
      seeds
  in
  let _ = List.map ~f:(fun s -> printf "%d\n" s) destinations in

  (* printf "%d\n" ((List.nth destinations 3) |> Twutils.strip_option 0) *)
  (* match List.nth maps 0 |> Twutils.strip_option ("", []) with *)
  (* let _ = List.map ~f:dump_map maps in *)
  ()

(*
  - read list of seeds

  - Read next map <- assumes next map is the right one (i.e. maps in
  right sequence).

    - for each seed (or current destination) - find the next destination

    - Continue until no more maps

  - Find the lowest destination
*)
