(* Day 1 *)

open Core

let input = "../input/test_01"

let match_digits lo hi c =
  if Twutils.is_digit c then
    let n = Stdlib.Char.code c - 48 in
    if phys_equal lo 0 then (n, n) else (lo, n)
  else
    (lo, hi)

let rec handle_line lo hi l =
  match Twutils.explode l with
  | [] ->
        let _ = printf "%d %d\n" lo hi in
        (lo, hi)
  | c::cs ->
    let (lo', hi') = match_digits lo hi c in
    handle_line lo' hi' (Twutils.implode cs)

let rec handle_lines acc lines =
  match lines with
  | [] -> acc
  | l::ll ->
    let (lo, hi) = handle_line 0 0 l in
    let a = lo * 10 + hi in
    let acc' = acc + a in
    handle_lines acc' ll

let () =
  let lines = In_channel.read_lines input in
  let m = handle_lines 0 lines in
  printf "%d\n" m
