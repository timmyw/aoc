(* Day 2a *)

open Core

let input = "../input/02"

(* 12 red cubes, 13 green cubes, and 14 blue  *)
(* let max_cubes = (12, 13, 14) *)

let is_game_possible (game, (r,g,b)) = if r <= 12 && g <= 13 && b <= 14 then game else 0

let extract_game s =
  let ss = String.split ~on:' ' s in
  match ss with
  | _ :: game_nums :: _ -> Int.of_string game_nums
  | _ -> 0

let strip str =
  let str = Str.replace_first (Str.regexp "^ +") "" str in
  Str.replace_first (Str.regexp " +$") "" str

(* 3 blue *)
let split_colour_play s =
  let ss = String.split ~on:' ' s in
  match ss with
  | cnts :: colour :: _ -> (Int.of_string cnts, colour)
  | _ -> (0, "UNK")

let max_tuple (x1, x2, x3) (y1, y2, y3) =
  (max x1 y1, max x2 y2, max x3 y3)

(*  3 blue, 4 red *)
let parse_game_plays l =
  let plays = List.map (String.split ~on:',' l) ~f:strip in
  List.fold
    ~init:(0,0,0)
    ~f:(fun acc s ->
      let (x, c) = split_colour_play s in
      let current_play = (
          (if String.equal c "red" then x else 0),
          (if String.equal c "green" then x else 0),
          (if String.equal c "blue" then x else 0)) in
      max_tuple acc current_play)
    plays

(* Parse each line and store the max of each colour *)
let max_game_lines l =
  let l' = Twutils.head l in
  let plays = List.map (String.split ~on:';' l') ~f:parse_game_plays in
  List.fold ~init:(0,0,0) ~f:(fun acc p -> max_tuple acc p) plays

(* Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green *)
let parse_game l =
  let l2 = String.split ~on:':' l in
  match l2 with
  | games :: plays -> (extract_game games,
                       max_game_lines plays)
  | _ -> (0, (0,0,0))

let rec handle_lines acc lines =
  match lines with
  | [] -> acc
  | l::ll ->
     let acc' = acc + is_game_possible (parse_game l) in
     handle_lines acc' ll

let () =
  let lines = In_channel.read_lines input in
  let m = handle_lines 0 lines in
  printf "%d\n" m
