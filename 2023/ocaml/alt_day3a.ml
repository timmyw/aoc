(* Day 3a *)

open Core

let input = "../input/test_03"

(*
467..114..
...*......
..35..633.
......#...
617*......
.....+.58.
..592.....
......755.
...$.*....
.664.598..
*)

let store_num_if_we_have_one row_num col_num nums bld =
  if String.equal bld "" then
    (nums, "")
  else
    let start_col = col_num - String.length bld in
    ((row_num, start_col, bld) :: nums, "")

let store_symbol
      (row_num:int)
      (col_num:int)
      (syms:(int * int) list)
    (* -> (int * int) list *)
  = (row_num, col_num) :: syms

let rec parse_line
          (row_num:int)
          (col_num:int)
          (nums:(int * int * string) list)
          (syms:(int * int) list)
          (bld:string)
          (l:string) =
  match l with
  | "" -> (* TODO: deal with built number *)
     (nums, syms)
  | _  -> let first = String.get l 0 in
          let rest = Twutils.right l (String.length l - 1) in
          if Twutils.is_digit first then
            (* start or part of a number *)
            let bld' = Twutils.suffix_char bld first in
            parse_line row_num (col_num+1) nums syms bld' rest
          else (* Not a digit *)
            let (nums', bld') = store_num_if_we_have_one row_num col_num nums bld in
            let syms' = if phys_equal first '.'
                        then syms
                        else store_symbol row_num col_num syms in
            parse_line row_num (col_num+1) nums' syms' bld' rest

(*
  parse all numbers and symbols
  store start and end coords of numbers and symbols
*)
let rec handle_lines (row_num:int)
          (nums:(int * int * string) list)
          (syms:(int * int) list)
          (lines:string list) =
  match lines with
  | [] -> (nums, syms)
  | l::ll ->
     let (nums', syms') = parse_line row_num 0 nums syms "" l in
     handle_lines (row_num+1) nums' syms' ll

(* let make_coords x y s = *)
(*   let rx = x + String.length s in *)
(*   let std_coords= [ *)
(*     (x-1, y); (x-1, y-1); (x-1, y+1); *)
(*     (rx+1, y); (rx+1, y-1); (rx-1, y+1) ] in *)
(*   let len_coords = List.concat (List.mapi ~f:(fun i _ -> [(x+i, y); (x+i, y+1)] ) (Twutils.explode s)) in *)
(*   List.concat [ std_coords; len_coords ] *)

let find_and_extract_number row col nums =
  let n = List.find nums
            ~f:(fun (row',col',s) ->
              (* if we are on the same row and the col is inbetween the start column and the end of
                 the string column *)
                if (col >= col')
                   && (col <= col' + (String.length s) -1)
                   && phys_equal row' row
                then
                  let _ = printf "FOUND: (%d,%d) %d %d %s\n" row col row' col' s in
                  true
                else
                  false) in
  match n with
  | Some (_,_,s) -> Int.of_string s
  | None         -> 0


(*
........
..$123..
........
..123$..
*)
let check_symbol acc (x,y) nums =
  let _ = printf "SYM: %d %d\n" x y in
  let acc' =
    (find_and_extract_number (x-1) y nums) +
      (find_and_extract_number (x+1) y nums) +
      (find_and_extract_number x     (y-1) nums) ::
      (find_and_extract_number x     (y+1) nums) ::
      (find_and_extract_number (x-1) (y-1) nums) ::
      (find_and_extract_number (x-1) (y+1) nums) ::
      (find_and_extract_number (x+1) (y-1) nums) ::
      (find_and_extract_number (x+1) (y+1) nums) :: []
  in acc' :: acc

let unique (l:int list) : (int list) =
  let rec aux l acc =
    match l with
    | []     -> List.rev acc
    | h :: t ->
        if List.mem acc h ~equal:(fun x y -> phys_equal x y)
        then aux t acc
        else aux t (h :: acc)
  in
  aux l []

let () =
  let lines = In_channel.read_lines input in
  let (nums, syms) = handle_lines 0 [] [] lines in
  (* let _ = List.map ~f:(fun (x,y,s) -> printf "%d,%d -> %s\n" x y s) nums in *)
  (* let _ = List.map ~f:(fun (x,y) -> printf "%d,%d\n" x y) syms in *)
  let m = List.concat (List.fold ~init:[] ~f:(fun acc s -> check_symbol acc s nums) syms) in
  let d = List.fold ~init:0 ~f:(fun acc i -> acc + i) (unique m) in
  printf "%d\n" d
