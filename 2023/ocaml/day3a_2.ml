(* Day 3a - Take two *)

open Core

let input = "../input/test_03"

(*
467..114..
...*......
..35..633.
......#...
617*......
.....+.58.
..592.....
......755.
...$.*....
.664.598..
*)

type board = int list list

let rec make_row l row =
  match l with
  | "" -> row
  | l' -> let row' = append row (Twutils.sub_str l' 0 1) in
          make_row (Twutils.right l' ((length l') - 1))

let rec make_board ll board =
  match ll with
  | [] -> board
  | l::ls -> let row = make_row l [] in
            let board' = append board row in
            make_board ls board'

(* let process_board x y max_x max_y ll = *)
(*   let (x', y') = (if x > max_x then 0 else x + 1, *)

let () =
  let lines = In_channel.read_lines input in
  let board = make_board lines [[]] in
    (* process_board 0 0 (length (lines !! 0)) (length lines) lines *)
  printf "%d\n" 0
