module Lib
  ( substring
  , substr
  ) where

substring s 0 j = take j s
substring s i j =
  take j (drop i s)

substr s j = drop j s
