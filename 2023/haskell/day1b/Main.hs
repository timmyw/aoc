{- day 1b -}

module Main (main) where

import Debug.Trace
import Data.Char

import Lib

input :: String
input = "../input/01"

digitWords = [ ( "zero", 0 ), ( "one", 1 ), ( "two", 2 )
             , ( "three", 3 ), ( "four", 4 ), ( "five", 5 )
             , ( "six", 6 ), ( "seven", 7 ), ( "eight", 8 )
             , ( "nine", 9 ) ]

matchString :: String -> Int
matchString s = foldr (\(w,i) acc ->
                         let sub_s = substring s 0 (length w) in
                         if w == sub_s
                         then i
                         else acc) (-1) digitWords
-- (trace $ w ++ " " ++ (show $ length w) ++ " " ++ sub_s ++ " " ++ (show $ length sub_s) ++ " -> " ++ (show (w == sub_s)))

getRightDigit :: String -> Int -> Int
getRightDigit s (-1) = 0
getRightDigit s i =
  let c = s !! i in
    if isDigit c
    then (ord c) - 48
    else if word > 0
         then word
         else getRightDigit s (i-1)
  where word = matchString (substr s i)

getLeftDigit :: String -> Int -> Int
getLeftDigit s i =
  if i >= length s
  then 0
  else
    let c = s !! i in
      if isDigit c
      then (ord c) - 48
      else if word > 0
           then word
           else getLeftDigit s (i+1)
  where word = matchString (substring s i ((length s)-i))

-- getRightDigit :: String -> Char
-- getRightDigit [] = '0'
-- getRightDigit s =
--   let (c, cc) = (last s, init s) in
--     if isDigit c
--     then c
--     else getRightDigit cc

-- getDigits :: String -> (Char, Char)
getDigits s =
  let l = getLeftDigit s 0 in
    let r = getRightDigit s ((length s)-1) in
  (l, r)

main :: IO ()
main = do
  content <- readFile input
  let ll = lines content
  let digits = map (\(x,y) -> x * 10 + y) $ map getDigits ll
 -- let digits = map getDigits ll
  -- print $ zip ll digits
  print $ sum digits
