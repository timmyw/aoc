{- day 2a -}

module Main (main) where

import Data.Char
import qualified Data.Map.Strict as M
import Data.List.Split
import Debug.Trace

import Lib

input :: String
input = "../input/test_02"

{-
Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green
-}

gamePlays :: String -> (String, String)
gamePlays l =
  let sp = splitOn ":" l in
  (head sp, sp !! 1)

extractPlays :: String -> [String]
extractPlays = splitOn ";"

maxColours :: [(String, Int)]
maxColours = M.fromList [("red", 12), ("green", 13), ("blue", 14)]

compareCounts :: Maybe Int -> Int -> Bool
compareCounts mc c
  | (Just cnt) c = cnt <= c
  | None _       = False


isPlayable :: (String, Int) -> Bool
isPlayable (colour, count) =
  let maxc = M.lookup colour maxColours in
    compareCounts maxc count

testGame :: String -> Int -- return Game number if playable, zero otherwise
testGame l =
  let (game, plays) = gamePlays l in
    let playSteps = extractPlays plays in
      if all isPlayable playSteps
      then getGameNumber game
      else 0
  where
    getGameNumber s =
      let ss = words s in
        read (ss !! 1) :: Int
    -- extractPlays ps = split
    -- isPlayable playSteps = -- ps = ["6 red, 1 blue, 3 green", "2 blue, 1 red, 2 green"]
    --   let colours = extractColours playSteps in
    --     let cs = splitOn "," s in
    --       map (\c -> let es = splitOn " " c in
    --                    (head es, read (es !! 1) :: Int)) cs

-- createMap :: [(String, Int)] ->
-- createMap = M.fromList

main :: IO ()
main = do
  content <- readFile input
  let ll = lines content
  let playable = map testGame ll
  print playable
  -- let digits = map (\(x,y) -> x * 10 + y) $ map getDigits ll
  -- let digits = map getDigits ll
  -- print $ zip ll digits
  -- print $ sum digits
