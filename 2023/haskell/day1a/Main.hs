module Main (main) where

import Debug.Trace
import Data.Char

input :: String
input = "../input/01"

getLeftDigit :: String -> Char
getLeftDigit (l:ll) =
  if isDigit l then l else getLeftDigit ll
getLeftDigit _ = '0'

getRightDigit :: String -> Char
getRightDigit [] = '0'
getRightDigit s =
  let (c, cc) = (last s, init s) in
    if isDigit c
    then c
    else getRightDigit cc

-- getDigits :: String -> (Char, Char)
getDigits s =
  let l = getLeftDigit s in
    let r = getRightDigit s in
  ((ord l) - 48, (ord r) - 48)

main :: IO ()
main = do
  content <- readFile input
  let ll = lines content
  let digits = map (\(x,y) -> x * 10 + y) $ map getDigits ll
  print digits
  print $ sum digits
