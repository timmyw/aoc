/*
  day 3a
*/

#include <cctype>
#include <iostream>
#include <fstream>
#include <iterator>
#include <numeric>

/*
467..114..
...*......
..35..633.
......#...
617*......
.....+.58.
..592.....
......755.
...$.*....
.664.598..


*/

const std::string input = "../input/03";

int row_len = 0;
int col_len = 0;

std::vector<std::string> load_file() {
  std::vector<std::string> lines;
  std::ifstream fd(input);
  if (fd.is_open()) {
    std::string line;
    while (getline(fd, line)) {
      lines.push_back(line);
    }
    fd.close();
  }
  return lines;
}

char *convert_vector_to_array(std::vector<std::string> lines) {
  char *a = new char[row_len * lines.size() + 1];
  char *p = a;
  for (auto i = lines.begin(); i != lines.end(); i++) {
    for (int j = 0; j < (*i).length(); j++) {
      *p++ = (*i)[j];
    }
  }

  *p = 0;
  return a;
}

inline char get_board(int row, int col, const char *p) {
  return p[col + row * row_len];
}

int find_number(int row, int col, const char *p, int dir_row, int dir_col) {
  std::string bld = "";
  char c = get_board(row, col, p);
  std::cout << "FIRST:" << row << "," << col << " : " << c << " - " << " (" << dir_col << ") "
            << std::isdigit(c) << std::endl;
  if (!std::isdigit(c))
    return 0;

  // first try move in the opposite direction until we move off the board or
  // encounter a non digit.
  if (0) {
  while (std::isdigit(c)) {
    col -= dir_col;

    // While we are still on the board, get the char
    if (row >= 0 && row < row_len && col >= 0 && col < col_len) {
      c = get_board(row, col, p);
      if (!std::isdigit(c)) col += dir_col;
      std::cout << "Check char:" << col << " :" << c << std::endl;
    }
    else { // We're off the board
      col += dir_col; // Reverse the change
      break;
    }
  }
  }

  c = get_board(row, col, p);
  std::cout << "AFTER REVERSE:" << row << "," << col << " : " << c << " - " << " (" << dir_col << ") "
            << std::isdigit(c) << std::endl;

  while (std::isdigit(c)) {
    std::cout << "next:" << row << "," << col << " : " << c << std::endl;
    if (dir_col > 0)
      bld += c;
    else
      bld = c + bld;
    row += dir_row;
    col += dir_col;
    if (row >= 0 && row < row_len && col >= 0 && col < col_len) {
      c = get_board(row, col, p);
    }
    else {
      c = '.'; // Force a break from the lopo
    }
  }
  /// std::cout << "bld:" << atoi(bld.c_str()) << std::endl;
  return (bld == "" ? 0 : atoi(bld.c_str()));
}

int main(int argc, char *argv[]) {
  std::vector<int> nums;
  auto lines = load_file();
  row_len = lines[0].length();
  col_len = lines.size();
  std::cout << "Board:" << row_len << " by " << col_len << std::endl;
  auto board = convert_vector_to_array(lines);
  /*
    Scan through line by line
   */

  int num;
  for (int col = 0; col < col_len; col++)
    for (int row = 0; row < row_len; row++) {
      char c = get_board(row, col, board);
      if (!std::isdigit(c) && c != '.') { // symbol
                                          // scan arouund to find a number
        std::cout << row << "," << col << " : " << c << std::endl;
        if (col > 0) { // look left
          // std::cout << "left" << std::endl;
          if (num = find_number(row, col - 1, board, 0, -1); num > 0)
            nums.push_back(num);
        }
        if (col < col_len - 1) { // look right
          // std::cout << "right" << std::endl;
          if (num = find_number(row, col+1, board, 0, 1); num > 0)
            nums.push_back(num);
        }
        if (row > 0) { // look above
          int tr = row - 1;
          if (!std::isdigit(get_board(tr, col, board))) { // No digit directly above
            if (num = find_number(tr, col - 1, board, 0, -1); num > 0)
              nums.push_back(num);
            if (num = find_number(tr, col + 1, board, 0, 1); num > 0)
              nums.push_back(num);
          } else {
            // Part of a number directly above
            int c = col;
            std::cout << "looking directly above from " << c << ":" << get_board(tr, c, board) << std::endl;
            while (c > 0 && isdigit(get_board(tr, c, board))) { // Keep moving left
              c--;
              std::cout << "left " << c << ":" << get_board(tr, c, board) << std::endl;
            }
            if (num = find_number(tr, c+1, board, 0, 1); num > 0)
              nums.push_back(num);
          }
        }
        if (row < row_len - 1) { // look below
          if (!std::isdigit(get_board(row+1, col, board))) { // No digit directly below
            if (num = find_number(row + 1, col - 1, board, 0, -1); num > 0)
              nums.push_back(num);
            if (num = find_number(row + 1, col + 1, board, 0, 1); num > 0)
              nums.push_back(num);
          } else {
            int c = col;
            while (c > 0 && isdigit(get_board(row + 1, c, board)))
              c--;
            if (num = find_number(row + 1, c+1, board, 0, 1); num > 0)
              nums.push_back(num);
          }
        }
      }
    }

  auto last = std::unique(nums.begin(), nums.end());
  nums.erase(last, nums.end());
  for (auto i = nums.begin(); i != nums.end(); i++) {
    std::cout << "num:" << (*i) << std::endl;
  }
  auto result = std::reduce(nums.begin(), nums.end());
  std::cout << "sum:" << result << std::endl;

  return 0;
}
