/*
  day3a_b
 */
#include<string>

const std::string input = "../input/03";

std::vector<std::string> load_file() {
  std::vector<std::string> lines;
  std::ifstream fd(input);
  if (fd.is_open()) {
    std::string line;
    while (getline(fd, line)) {
      lines.push_back(line);
    }
    fd.close();
  }
  return lines;
}

int main(int argc, char* argv[]) {
  return 0;
}
