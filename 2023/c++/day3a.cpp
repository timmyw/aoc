/*
  day 3a
*/

#include <cctype>
#include <vector>
#include <iostream>
#include <fstream>
#include <iterator>
#include <numeric>
#include <tuple>
#include <algorithm>

/*
467..114..
...*......
..35..633.
......#...
617*......
.....+.58.
..592.....
......755.
...$.*....
.664.598..

.....
...$.
222$.
.....
$33$.
.....
.*...
..111
999..
.44..
...$.

*/

std::string input = "../input/03";

int max_x = 0;
int max_y = 0;

#if __clang_major__ < 15 || (defined(linux) && linux == 1)
template<class ForwardIt>
ForwardIt unique(ForwardIt first, ForwardIt last)
{
    if (first == last)
        return last;

    ForwardIt result = first;
    while (++first != last)
        if (!(*result == *first) && ++result != first)
            *result = std::move(*first);

    return ++result;
}
#endif // __clang_major__ < 15 || (defined(linux) && linux == 1)


void put_board(int x, int y, char *p, char c) {
  // std::cout << x << "," << y << ":" << x + y * max_x << std::endl;
  p[x + y * max_x] = c;
}

inline char get_board(int x, int y, const char *p)
{
  return p[x + y * max_x];
}

std::vector<std::string> load_file() {
  std::vector<std::string> lines;
  std::ifstream fd(input);
  if (fd.is_open()) {
    std::string line;
    while (getline(fd, line)) {
      lines.push_back(line);
    }
    fd.close();
  }
  return lines;
}

char *convert_vector_to_array(std::vector<std::string> lines) {
  char *a = new char[max_x * max_y + 1];
  int y = 0;
  for (auto i = lines.begin(); i != lines.end(); y++, i++) {
    for (int x = 0; x < (*i).length(); x++)
      put_board(x, y, a, (*i)[x]);
  }
  return a;
}

void dump_board(const char *board) {
  std::cout << "Board:" << max_x << " by " << max_y << std::endl;
  for (int y = 0; y < max_y; y++) {
    for (int x = 0; x < max_x; x++)
      std::cout << get_board(x, y, board);
    std::cout << std::endl;
  }
}


int find_number(int x, int y, const char *p,
                int dir_x, int dir_y,
                bool reverse_first = false) {
  auto dump = [&](std::string msg, char c_) {
    std::cout << msg << "(" << x << "," << y
              << ") : " << c_ << " - " << " (" << dir_x << ") "
              << (std::isdigit(c_)?"true":"false") << std::endl;
  };
  auto valid_coords = [&](int xx, int yy) { return (yy >= 0 && yy < max_y && xx >= 0 && xx < max_x); };

  std::string bld = "";
  char c = get_board(x, y, p);
  dump("FIRST", c);
  if (!std::isdigit(c))
    return 0;

  // first try move in the opposite direction until we move off the
  // board or encounter a non digit.
  while (reverse_first && dir_x != 0  && std::isdigit(c)) {
    x -= dir_x;
    // While we are still on the board, get the char
    if (valid_coords(x,y)) {
      c = get_board(x, y, p);
      std::cout << "check char: (" << x << "," << y << ") :" << c << std::endl;
      if (!std::isdigit(c)) {
        x += dir_x;
        std::cout << "not a digit (" << x << "," << y << std::endl;
      }
    } else { // We're off the board
      // x += dir_x;                               // Reverse the change
      reverse_first = false;
    }
  }

  c = get_board(x, y, p);
  dump("AFTER REVERSE", c);

  while (std::isdigit(c)) {
    std::cout << "next:" << x << "," << y << " : " << c << std::endl;
    if (dir_x > 0)
      bld += c;
    else
      bld = c + bld;
    x += dir_x; y += dir_y;
    if (valid_coords(x,y)) {
      c = get_board(x, y, p);
      std::cout << "next_:" << x << "," << y << " : " << c << std::endl;
    } else {
      std::cout << "not valid coords" << std::endl;
      break;
    }
  }
  return (bld == "" ? 0 : atoi(bld.c_str()));
}

/*
- Check straight above/below
- If digit
  - Go left until no digit
  - Scan number right
- else
  - Check above/below left and go left
  - Check above/below right and go right
*/

int check_position(int x, int y, const char *board, std::vector<int> &nums) {
  char c = get_board(x, y, board);
  if (!std::isdigit(c) && c != '.') { // symbol
    std::cout << "(" << x << "," << y << ") : " << c << std::endl;

    if (x > 0) { if (int num = find_number(x - 1, y, board, -1, 0); num > 0) nums.push_back(num); } // look left

    if (x < max_x - 1) { if (int num = find_number(x + 1, y, board, 1, 0); num > 0) nums.push_back(num); }  // look right

    if (y > 0) {
      /*
        - Check straight above/below
        - else
          - Check above/below left and go left
          - Check above/below right and go right
      */
      int ty = y - 1; int tx = x;
      char c1 = get_board(tx, ty, board);
      if (std::isdigit(c1)) {
        while (std::isdigit(c1)) {
          c1 = get_board(--tx, ty, board);
        }
        std::cout << "tx:" << tx << ", ty:" << ty << std::endl;
        if (int num = find_number(tx+1, ty, board, 1, 0); num > 0) nums.push_back(num);
      } else {
        /*
          - Check above/below left and go left
          - Check above/below right and go right
        */
        if (x > 0 && std::isdigit(get_board(x-1, ty, board))) {
          if (int num = find_number(x-1, ty, board, -1, 0); num > 0) nums.push_back(num);
        }
        if (x < max_x - 1 && std::isdigit(get_board(x+1, ty, board))) {
          if (int num = find_number(x+1, ty, board, 1, 0); num > 0) nums.push_back(num);
        }
      }
    }

    // check below left and right
    if (y < max_y - 1) {
      if (int num = find_number(x+1, y+1, board, 1, 0, true); num > 0)
        nums.push_back(num);
      else
        if (int num = find_number(x-1, y+1, board, -1, 0, true); num > 0)
          nums.push_back(num);
    }
  }

  return 0;
}

int main(int argc, char *argv[]) {
  if (argc > 1)
    input = argv[1];

  std::vector<int> nums;
  auto lines = load_file();
  max_x = lines[0].length(); // Assume all lines same length
  max_y = lines.size();      // Number of lines
  auto board = convert_vector_to_array(lines);

  dump_board(board);

  for (int y = 0; y < max_y; y++)
    for (int x = 0; x < max_x; x++) {
      check_position(x, y, board, nums);
    }

  // std::cout << "check unique" << std::endl;
  // for (auto i = nums.begin(); i != nums.end(); i++) {
  //   auto j = std::find(i, nums.end(), *i);
  //   if (j != nums.end()) {
  //     auto k = std::find(j, nums.end(), *i);
  //     if (k != nums.end()) {
  //       std::cout << (*i) << std::endl;
  //     }
  //   }
  // }

  auto before_uniq = nums.size();
// #if __clang_major__ < 15 || (defined(linux) && linux == 1)
//   auto last = unique(nums.begin(), nums.end());
// #else
//   auto last = std::unique(nums.begin(), nums.end());
// #endif
  // nums.erase(last, nums.end());
  auto after_uniq = nums.size();

  for (auto i : nums)
    std::cout << "num:" << i << std::endl;
  auto result = std::reduce(nums.begin(), nums.end());
  std::cout << "sum:" << result << std::endl;

  // std::cout << "before:" << before_uniq << std::endl
  //           << "after:" << after_uniq << std::endl;

  return 0;
}
