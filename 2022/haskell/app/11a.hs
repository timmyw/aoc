module Main (main) where

import Foreign.Marshal (moveBytes)
import Distribution.InstalledPackageInfo (InstalledPackageInfo(frameworkDirs))
--import Debug.Trace

data Monkey = Monkey {
  visited :: Integer
  , num :: Integer
  , items :: [Integer]
  , op :: String
  , operand :: String
  , test_div :: Integer
  , true_route :: Integer
  , false_route :: Integer
  } deriving (Show)

test_monkeys :: [Monkey]
test_monkeys = [
  Monkey { visited = 0, num = 0, items = [79, 98], op = "*", operand = "19", test_div = 23, true_route = 2, false_route = 3 }
  , Monkey { visited = 0, num = 1, items = [54, 65, 75, 74], op = "+", operand = "6", test_div = 19, true_route = 2, false_route = 0 }
  , Monkey { visited = 0, num = 2, items = [79, 60, 97], op = "*", operand = "old", test_div = 13, true_route = 1, false_route = 3 }
  , Monkey { visited = 0, num = 3, items = [74], op = "+", operand = "3", test_div = 17, true_route = 0, false_route = 1 }
  ]

dumpMonkeyS :: Monkey -> IO ()
dumpMonkeyS m =
  putStrLn $ show (num m) ++ ": " ++ dumpItems
  where dumpItems = foldMap (\i -> show i ++ ",") (items m)

dumpMonkeys :: [Monkey] -> IO ()
dumpMonkeys = foldMap dumpMonkeyS

processMonkeyItem :: Monkey -> Integer -> (Integer, Integer)
processMonkeyItem m item = (item', if div_result == 0 then true_route m else false_route m)
  where operand' = if operand m == "old" then item else (read (operand m) :: Integer)
        op' = if op m == "*" then (*) else (+)
        item' = op' item operand' `div` 3
        div_result = item' `mod` test_div m

processMonkey :: Monkey -> (Integer, [(Integer, Integer)])
processMonkey m = (num m, map (processMonkeyItem m) (items m))

processMonkeyResults :: [Monkey] -> (Integer, [(Integer, Integer)]) -> [Monkey]
processMonkeyResults mm (_,[])      = mm
processMonkeyResults mm (n, r:rr) = processMonkeyResults (applyChanges n r mm) (n,rr)

applyChanges :: Integer -> (Integer, Integer) -> [Monkey] -> [Monkey]
applyChanges me (item, n) = map
                               (\m -> if num m == me then
                                        m { items = [] }
                                        else
                                        if num m == n then addToItems m item else m )

addToItems :: Monkey -> Integer -> Monkey
addToItems m item = m { items = item : items m }

--processMonkeys :: [Monkey] -> [Monkey] -> [Monkey]
-- processMonkeys [] work = work
-- processMonkeys (m:mm) work = processMonkeys mm work'
--   where singleMonkey m' = processMonkeyResults work $ processMonkey m'
--         work' = singleMonkey work
  -- where work' =  map singleMonkey work
  --       singleMonkey m' = processMonkeyResults work $ processMonkey m'

main :: IO ()
main = do
  putStrLn "11a"
  dumpMonkeys test_monkeys
  --let monkeys = foldl (\acc m -> (processMonkeyResults test_monkeys $ processMonkey m) : acc) [] test_monkeys
  -- let monkey = map (\m -> processMonkeyResults test_monkeys $ processMonkey m) test_monkeys
  let working_monkeys = test_monkeys
  let monkey =  processMonkeyResults test_monkeys $ processMonkey (head test_monkeys)
  -- accumulator must be the working monkey set.  We fold down
  -- another working monkey set
  let starter_monkeys = map (\x -> x-1) $ take (length working_monkeys) [1..100]
  let monkeys = foldr (\m working -> processMonkeyResults working $ processMonkey (working !! m)) working_monkeys starter_monkeys
  dumpMonkeys monkeys
