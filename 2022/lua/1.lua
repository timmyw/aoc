
input = "../input/input1"

function check_new_max (v)
     for i = 1,3 do
          if v > max[i] then
               t = max[i]
               max[i] = v
               check_new_max(t)
               return
          end
     end
end

subtotal = 0
max = {0;0;0}
for line in io.lines(input) do
     print(line)
     if line == "" then
          -- Change
          subtotal = 0
     else
          subtotal = subtotal + tonumber(line)
     end

     check_new_max(subtotal)
end

print("Three max ", max[1] + max[2] + max[3])
