(* Day 2a *)

open Core

let input = "../input/input2"

let play_score c =
  match c with
  | 'X' -> 1
  | 'Y' -> 2
  | 'Z' -> 3
  | _ -> 0

let result_score them me =
  match (them, me) with
  | ('A', 'X') -> 3
  | ('B', 'Y') -> 3
  | ('C', 'Z') -> 3
  | ('C', 'X') -> 6
  | ('A', 'Y') -> 6
  | ('B', 'Z') -> 6
  | _ -> 0

let rec handle_lines score lines =
  match lines with
  | [] -> score
  | l::ll ->
     let (them, me) = (l.[0], l.[2]) in
     let new_score = score + play_score me + result_score them me in
     handle_lines new_score ll

let () =
  let lines = In_channel.read_lines input in
  let score = handle_lines 0 lines in
  printf "score: %d\n" score
