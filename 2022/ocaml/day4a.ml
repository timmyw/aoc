
(* Day 4a *)

open Core

let input = "../input/input4"

let split (l: string) : (string * string) =
  let ls = String.split_on_chars ~on:[','] l in
  match ls with
    | s1 :: s2 :: _ -> (s1, s2)
    | _ -> ("", "")

(* dd-dd *)
let split_span (s: string) : (int * int) =
  let ss = String.split_on_chars ~on:['-'] s in
  match ss with
    | i1 :: i2 :: _ -> (Int.of_string i1, Int.of_string i2)
    | _ -> (0, 0)

let span_contains (s1, e1) (s2, e2) =
  if s1 >= s2 && e1 <= e2 then
    true
  else
    false

let rec handle_lines accum lines =
  match lines with
  | [] -> accum
  | l::ll ->
     let (span1s, span2s) = split l in
     (* let _ = printf "%s - %s\n" span1s span2s in *)
     let (span1_start, span1_end) = split_span span1s in
     let (span2_start, span2_end) = split_span span2s in
     let overlap = span_contains (span1_start, span1_end) (span2_start, span2_end) || span_contains (span2_start, span2_end) (span1_start, span1_end) in
     let new_accum = accum + (if overlap then 1 else 0) in
     handle_lines new_accum ll

let () =
  let lines = In_channel.read_lines input in
  let count = handle_lines 0 lines in
  printf "count: %d\n" count
