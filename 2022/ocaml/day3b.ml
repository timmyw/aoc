(* Day 3b *)

open Core

let input = "../input/input3"

let rec match_common a b c =
  if String.compare a "" = 0 then
    '_'
  else
    if String.contains b a.[0] && String.contains c a.[0] then
      a.[0]
    else
      match_common (String.sub a ~pos:1 ~len:(String.length a - 1)) b c

let translate_priority c =
  let i = Stdlib.Char.code(c) in
  if Char.compare c 'a' > -1 && Char.compare c 'z' < 1 then
    i - 96
  else
    i - 64 + 26

let rec handle_lines score lines =
  match lines with
  | [] -> score
  | a::b::c::ll ->
     let common = match_common a b c in
     (* let _ = printf "common:%c\n" common in *)
     let priority = translate_priority common in
     handle_lines (score + priority) ll
  | _ :: ll -> handle_lines score ll

let () =
  let lines = In_channel.read_lines input in
  let score = handle_lines 0 lines in
  printf "score: %d\n" score
