(* Day 6b *)

open Core
open String

let input = "../input/input6"

let chunk_len = 14
let last_x s =
  let _ = printf "last_x: %d %d\n" (String.length s) chunk_len in
  Twutils.right s chunk_len

(*
   are all x characters different?
*)
let check (s : string) : bool =
  let make_sub s d =
    (Twutils.left s (d-1)) ^ (Twutils.right s (14-d)) in
  List.exists [1;2;3;4;5;6;7;8;9;10;11;12;13;14]
    (* ~init:false *)
    ~f:(fun i ->
        let ss = make_sub s i in
        (* let _ = printf "%d %s %c [%s|%s] \n" i ss s.[i-1] (Twutils.left s (i-1)) (Twutils.right s (4-i)) in *)
        String.contains ss s.[i-1]
      )

let process ((cur, idx, ret) : (string * int * int)) (ch : char) =
  if ret != 0 then
    (cur, idx, ret)
  else
    let cur' = last_x (cur ^ Char.escaped ch) in
    let new_ret : int = Core.min idx (if ret == 0 then 9999 else ret) in
    let ret' =  if check cur' then 0 else new_ret in
    (cur', idx+1, ret')

let () =
  let lines = In_channel.read_lines input in
  let line = Twutils.head lines in
  let (fin, _, x) = String.fold line
      ~init:(Twutils.left line (chunk_len - 1), 1, 0)
      ~f:(fun (cur, idx, ret) ch ->
          let (c', i', r') = process (cur, idx, ret) ch in
          (* let _ = printf "%s %2d %2d\n\n" c' i' r' in *)
          (c', i', r')
        )  in
  printf "%s %d\n" fin x
