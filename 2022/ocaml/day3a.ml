(* Day 3a *)

open Core

let input = "../input/input3"

let split l =
  let left = String.sub l ~pos:0 ~len:(String.length l / 2) in
  let right = String.sub l ~pos:(String.length l / 2) ~len:(String.length l / 2) in
  (left, right)

let rec match_common l r =
  if String.compare l "" = 0 then
    '_'
  else
    if String.contains r l.[0] then
      l.[0]
    else
      match_common (String.sub l ~pos:1 ~len:(String.length l - 1))
        r

let translate_priority c =
  let i = Stdlib.Char.code(c) in
  if Char.compare c 'a' > -1 && Char.compare c 'z' < 1 then
    i - 96
  else
    i - 64 + 26

let rec handle_lines score lines =
  match lines with
  | [] -> score
  | l::ll ->
     let (left, right) = split l in
     (* let _ = printf "%s\n%s\n" left right in *)
     let common = match_common left right in
     (* let _ = printf "common:%c\n" common in *)
     let priority = translate_priority common in
     handle_lines (score + priority) ll

let () =
  let lines = In_channel.read_lines input in
  let score = handle_lines 0 lines in
  printf "score: %d\n" score
