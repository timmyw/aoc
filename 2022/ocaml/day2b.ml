(* Day 2b *)

open Core

let input = "../input/input2"

let play_score c =
  match c with
  | 'X' -> 1
  | 'A' -> 1
  | 'Y' -> 2
  | 'B' -> 2
  | 'Z' -> 3
  | 'C' -> 3
  | _ -> 0

let get_lose c =
  match c with
  | 'A' -> play_score 'Z'
  | 'B' -> play_score 'X'
  | 'C' -> play_score 'Y'
  | _ -> 0

let get_draw c = play_score c + 3

let get_win c = match c with
  | 'A' -> play_score 'Y' + 6
  | 'B' -> play_score 'Z' + 6
  | 'C' -> play_score 'X' + 6
  | _ -> 0

let result_score them result =
  match result with
  | 'X' -> get_lose them        (* Lose *)
  | 'Y' -> get_draw them        (* Draw *)
  | 'Z' -> get_win them         (* Win *)
  | _ -> 0

let rec handle_lines score lines =
  match lines with
  | []    -> score
  | l::ll ->
     let (them, result) = (l.[0], l.[2]) in
     let round_result = result_score them result in
     let new_score = score + round_result in
     let _ = printf "%c %c = %d %d\n" them result round_result new_score in
     handle_lines new_score ll

let () =
  let lines = In_channel.read_lines input in
  let score = handle_lines 0 lines in
  printf "score: %d\n" score
