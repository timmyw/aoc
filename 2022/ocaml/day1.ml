(* Day 1 *)

open Core

let input = "../input/input1"

let rec handle_line (accum : int) (cur_max : int) (lines : string list) =
  match lines with
  | [] -> cur_max
  | l::ll -> if (String.compare l "" = 0) then
      let new_max = max accum cur_max in
      handle_line 0 new_max ll
      else
        let i = int_of_string l in
        handle_line (accum+i) cur_max ll

                 (* if l = "" then *)
  (*   else *)
  (*     let i = int_of_string l in *)
  (*     accum += i *)



let () =
  let lines = In_channel.read_lines input in
  let m = handle_line 0 0 lines in
  printf "%d\n" m
