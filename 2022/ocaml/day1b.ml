(* Day 1b *)

open Core

let input = "../input/input1"

let tuple_access (i : int) ((m1, m2, m3) : int * int * int) =
  match i with
  | 1 -> m1
  | 2 -> m2
  | 3 -> m3
  | _ -> -1

let tuple_set (index:int) (v:int) ((m1, m2, m3) : int * int * int) =
  match index with
  | 1 -> (v, m2, m3)
  | 2 -> (m1, v, m3)
  | 3 -> (m1, m2, v)
  | _ -> (m1, m2, m3)

(* check the supplied value i against each element of the tuple in order.  If it
   is larger than an element, replace it and check the rest of the tuple with
   original maximum *)
let tuple_max (i : int) (m : int * int * int) =
  let rec tuple_max_ j v m =
    if j > 3 then
      m
    else
      let k = tuple_access j m in
      if v > k then
        let m_ = tuple_set j v m in (* Store new max *)
        tuple_max_ (j+1) k m_       (* Check rest of tuple with old max *)
      else
        tuple_max_ (j+1) v m    (* Check the next tuple *)
  in
  tuple_max_ 1 i m

let rec handle_lines (accum : int)
          (cur_max : int * int * int)
          (lines : string list) =
  let new_max = tuple_max accum cur_max in
  match lines with
  | []    -> new_max                           (* Deal with last sum *)
  | l::ll -> if (String.compare l "" = 0) then (* Blank line *)
               handle_lines 0 new_max ll
             else
               let i = int_of_string l in      (* We have an int *)
               (* let _ = printf "%s %d\n" l i in *)
               handle_lines (accum+i) cur_max ll

let () =
  let lines = In_channel.read_lines input in
  let (m1, m2, m3) = handle_lines 0 (0,0,0) lines in
  printf "(%d, %d, %d) %d\n(%d, %d, %d) %d\n" 72240 69625 69092 210957 m1 m2 m3 (m1 + m2 + m3)

(* correct (pascal) version:
72240
69625
69092
Max 3 total 210957

*)
