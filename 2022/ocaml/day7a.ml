(* Day 7a *)
open Core

let input = "../input/test_input7"

type entry =
    | Dir of string * entry list
    | File of string * int

let change_directory cur dir =

let rec process (lines : string list) (cur_entry : entry)  =
  match lines with
  | [] -> cur_entry
  | l::ls ->
    let new_entry =
      match String.split ~on:' ' l with
      | "$" :: "cd" :: dir_name -> change_directory cur_entry dir_name
    in
    process ls new_entry

let () =
  let lines = In_channel.read_lines input in
  let dirs = process lines (Dir ("/", [])) in
  match dirs with
  | Dir (name, _) -> printf "%s\n" name
