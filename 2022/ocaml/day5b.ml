
(* Day 5b *)

open Core

let input = "../input/input5"

let starting_stack =
  [
    "RNFVLJSM"
  ; "PNDZFJWH"
  ; "WRCDG"
  ; "NBS"
  ; "MZWPCBFN"
  ; "PRMW"
  ; "RTNGLSW"
  ; "QTHFNBV"
  ; "LMHZNF"
  ]

(* let starting_stack_test = [ "ZN"; "MCD"; "P" ] *)

(*
    [D]
[N] [C]
[Z] [M] [P]
 1   2   3
*)

(*
[M] [H]         [N]
[S] [W]         [F]     [W] [V]
[J] [J]         [B]     [S] [B] [F]
[L] [F] [G]     [C]     [L] [N] [N]
[V] [Z] [D]     [P] [W] [G] [F] [Z]
[F] [D] [C] [S] [W] [M] [N] [H] [H]
[N] [N] [R] [B] [Z] [R] [T] [T] [M]
[R] [P] [W] [N] [M] [P] [R] [Q] [L]
 1   2   3   4   5   6   7   8   9 *)

(* move 2 from 2 to 1 *)
let split_line l =
  let elems = String.split ~on:' ' l in
  match elems with
  | _ :: cnt :: _ :: frm :: _ :: dest :: _ ->
     (Int.of_string cnt, Int.of_string frm, Int.of_string dest)
  | _ -> (0, 0, 0)

let replace_in_stack stack idx s =
  List.mapi stack ~f:(fun i x -> if Caml.(==) i idx then s else x)

let dump_stack ss =
  List.iteri ~f:(fun i s -> printf "%3d %s\n" (i+1) s) ss;
  print_endline ""

(* String.sub "abcdef" ~pos:1 ~len:3;; *)
let apply stack cnt froms tos =
  let org_str = List.nth_exn stack (froms-1) in
  let org_dest = List.nth_exn stack (tos-1) in
  let move_sub = String.sub org_str ~pos:((String.length org_str) - cnt) ~len:cnt in
  let new_org = String.sub org_str ~pos:0 ~len:((String.length org_str) - cnt) in
  let ns' = replace_in_stack stack (tos-1) (org_dest ^ move_sub) in
  let ns'' = replace_in_stack ns' (froms-1) new_org in
  ns''

let rec handle_lines stack lines =
  match lines with
  | [] -> stack
  | l::ll ->
     let (cnt, froms, tos) = split_line l in
     let _ = print_endline l in
     let new_stack = apply stack cnt froms tos in
     let _ = dump_stack stack in
     let _ = dump_stack new_stack in
     handle_lines new_stack ll

let dump_top stack = List.map
                       stack
                       ~f:(fun s -> String.sub
                                      s
                                      ~pos:((String.length s) - 1)
                                      ~len:1)

let () =
  let lines = In_channel.read_lines input in
  let end_stack = handle_lines starting_stack lines in
  let tops = dump_top end_stack in
  List.iter tops ~f:(fun s -> printf "%s" s)
