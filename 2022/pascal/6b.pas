{$mode objfpc}{$H+}
program aoc6b;

uses Sysutils, utils, Types, StrUtils;

const
   C_INPUT    = '../input/input6';
   header_len =  14;

function all_differ(s : string; chk : integer): boolean;
var
   i       : integer;
   m,s1,s2 : string;
begin
   for i := 1 to chk do
   begin
      m := copy(s, i, 1);
      s1 := copy(s, 1, i-1);
      s2 := copy(s, i+1, length(s));
      writeln(s1, ' ', m, ' ', s2, ' ', pos(m, s2), ' ');
      if (pos(m, s1) > 0) or (pos(m, s2) > 0) then
      begin
         all_differ := False;
         exit;
      end;
   end;
   all_differ := True;
end;

var
   fd_in : TextFile;
   line  : string;
   i     : integer;
   s     : string;
begin
   AssignFile(fd_in, C_INPUT);
   reset(fd_in);
   readln(fd_in, line); { should all be in one string }
   CloseFile(fd_in);

   i := header_len;
   while i < length(line) do
   begin
      s := copy(line, i-(header_len-1), header_len);
      writeln(line, ' ', s);
      if all_differ(s, header_len) then
         break;
      i := i + 1;
   end;

   writeln('Pos: ', i);
end.
