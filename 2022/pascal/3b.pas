{$mode objfpc}{$H+}
program aoc3b;

uses Sysutils, utils;

const
   C_INPUT   = '../input/input3';

function exist_in_string(haystack, needle : string) :boolean;
begin
   if pos(needle, haystack) > 0 then
      exist_in_string := True
   else
      exist_in_string := False
end;

function find_badge(l1, l2 , l3 : string): integer;
var j  :  integer;
begin
   for j := 1 to length(l1) do
   begin
      if exist_in_string(l2, copy(l1, j, 1)) and exist_in_string(l3, copy(l1, j, 1)) then
      begin
         find_badge := get_item_priority(l1[j]);
         break;
      end;
   end;
end;

var
   fd_in        : TextFile;
   l1, l2, l3 : string;
   priority  : integer;

begin
   AssignFile(fd_in, C_INPUT);
   reset(fd_in);
    priority := 0;

   while not eof(fd_in) do
   begin
      readln(fd_in, l1);
      readln(fd_in, l2);
      readln(fd_in, l3);
      writeln(l1);
      writeln(l2);
      writeln(l3);
      priority += find_badge(l1, l2, l3);
      writeln(priority);
   end;
   CloseFile(fd_in);
end.
