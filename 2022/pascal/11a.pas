{$mode objfpc}{$H+}
program aoc11a;

uses Sysutils, utils, Types, StrUtils, data11;

var
   round   : integer;

procedure init ();
begin
   round := 1;
end;

procedure dump_monkey (pm : PMonkey; full: Boolean);
var
   i : integer;
begin
   if full then
   begin
      writeln('Monkey ', pm^.num);
      write('  Items: ');
      for i := low(pm^.items) to high(pm^.items) do
         write(pm^.items[i], ', ');
      writeln();
      writeln('  Operator:', pm^.op, ' ', pm^.operand);
      writeln('  Test:', pm^.test_div);
   end
   else
   begin
      write('Monkey ', pm^.num, ': ');
      for i := low(pm^.items) to high(pm^.items) do
         write(pm^.items[i], ', ');
      writeln();
   end;
end;

type PArrayMonkey = ^TMonkeys;

procedure dump_monkeys (mm : TMonkeys; full: Boolean);
var
   i      : integer;
begin
   if full = True then
      writeln('Monkeys [', low(mm), ' -> ', high(mm), ']');
   for i := low(mm) to high(mm) do
      dump_monkey(@mm[i], full);
end;

procedure monkeys (p : TMonkeys);
begin
   writeln('Array [', low(p), '...', high(p), ']');
end;

procedure process_round (mm : TMonkeys );
var
   i,j           : integer;
   k, r, s, code : integer;
begin
   for i := low(mm) to high(mm) do
   begin
      for j := low(mm[i].items) to high(mm[i].items) do
      begin
         { Apply operation }
         if mm[i].operand = 'old' then
            k := mm[i].items[j]
         else
            val(mm[i].operand, k, code);
         if mm[i].op = '*' then
            mm[i].items[j] := mm[i].items[j] * k;
         if mm[i].op = '+' then
            mm[i].items[j] := mm[i].items[j] + k;

         { Divide by 3 }
         mm[i].items[j] := Trunc(mm[i].items[j] / 3);

         { Test }
         r := mm[i].items[j];
         delete(mm[i].items, j, 1);
         s := mm[i].false_route;
         if r mod mm[i].test_div = 0 then
            s := mm[i].true_route;
         writeln('monkey ', i, ' test: ', r, ' ', mm[i].test_div, ' ', r mod mm[i].test_div, ' moving to ', s);
         insert(r, mm[s].items, 0);
         dump_monkeys(test_monkeys, False);

      end;
   end;
end;

var
   i :  integer;
begin
   init();
   dump_monkeys(test_monkeys, False);
   for round := 1 to 1 do
   begin
      writeln('Round ', round);
      writeln('-----------');
      process_round(test_monkeys);
      dump_monkeys(test_monkeys, False);
   end;
end.
