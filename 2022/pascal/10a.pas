{$mode objfpc}{$H+}
program aoc10a;

uses Sysutils, utils, Types, StrUtils;

const
   C_INPUT = '../input/input10';

var
   cycle  : integer;
   signal : integer;
   x      : integer;

procedure init ();
begin
   x := 1;
   cycle := 0;
   signal := 0;
end;

procedure check_cycle ();
begin
   if (cycle = 20) or (cycle = 60) or (cycle = 100) or
      (cycle = 140) or (cycle = 180) or (cycle = 220) then
   begin
      signal := signal + x * cycle;
      writeln(cycle, ' ', x, ' ', x * cycle, ' ', signal);
   end;
end;

var
   fd_in     : TextFile;
   line      : string;
   bits      : TStringDynArray;
   cnt, code : integer;
begin
   init();
   AssignFile(fd_in, C_INPUT);
   reset(fd_in);
   while not eof(fd_in) do
   begin
      readln(fd_in, line);
      if line = 'noop' then
      begin
         cycle := cycle + 1;
         check_cycle ();
      end
      else
      begin
         bits := splitstring(line, ' ');
         val(bits[1], cnt, code);
         cycle := cycle + 1;
         check_cycle ();
         cycle := cycle + 1;
         check_cycle ();
         x := x + cnt;
      end;
   end;
   CloseFile(fd_in);
   writeln(signal);
end.
