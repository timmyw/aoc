{$mode objfpc}{$H+}
program aoc10b;

uses Sysutils, utils, Types, StrUtils;

const
   C_INPUT = '../input/input10';

var
   cycle  : integer;
   x      : integer;
   line      : string;

procedure init ();
begin
   x := 1;
   cycle := 0;
end;

procedure check_cycle ();
var lcyc : integer;
   s     : string;
begin
   lcyc := cycle mod 40;
   { write ('[', format('%20s', [line]), '] ', }
   {        format('%2d', [lcyc]), ' ', }
   {        format('%2d', [x]), ' '); }
   if abs (lcyc - x - 1) <= 1 then
      write ('#')
   else
      write ('.');
   if lcyc = 0 then
   begin
      writeln ();
   end;
   { writeln (); }
end;

var
   fd_in     : TextFile;
   bits      : TStringDynArray;
   cnt, code : integer;
begin
   init();
   AssignFile(fd_in, C_INPUT);
   reset(fd_in);
   while not eof(fd_in) do
   begin
      readln(fd_in, line);
      if line = 'noop' then
      begin
         cycle := cycle + 1;
         check_cycle ();
      end
      else
      begin
         bits := splitstring(line, ' ');
         val(bits[1], cnt, code);
         cycle := cycle + 1;
         check_cycle ();
         cycle := cycle + 1;
         check_cycle ();
         x := x + cnt;
      end;
   end;
   CloseFile(fd_in);
end.
