{$mode objfpc}{$H+}
program aoc6a;

uses Sysutils, utils, Types, StrUtils;

const
   C_INPUT =  '../input/test_input6';

function all_differ(s :  string): boolean;
var b : boolean;
begin
   b := True;
   if (s[1] = s[2]) or (s[1] = s[3]) or (s[1] = s[4]) then b := False;
   if (s[2] = s[1]) or (s[2] = s[3]) or (s[2] = s[4]) then b := False;
   if (s[3] = s[1]) or (s[3] = s[2]) or (s[3] = s[4]) then b := False;
   if (s[4] = s[1]) or (s[4] = s[2]) or (s[4] = s[3]) then b := False;
   all_differ := b;
end;

var
   fd_in : TextFile;
   line  : string;
   i     : integer;
begin
   AssignFile(fd_in, C_INPUT);
   reset(fd_in);
   readln(fd_in, line); { should all be in one string }
   CloseFile(fd_in);

   i := 4;
   while i < length(line) - 3 do
   begin
      if all_differ(copy(line, i-3, 4)) then
         break;
      i := i + 1;
   end;

   writeln('Pos: ', i);
end.
