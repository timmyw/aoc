{$mode objfpc}{$H+}
program aoc7a;

uses Sysutils, utils, Types, StrUtils;

const
   C_INPUT    = '../input/input7';

type TFile = record
               name : string;
               size : integer;
            end;

type
   PDirectory = ^TDirectory;
   TDirectory = record
                   name      : string;
                   all_size  : integer;
                   file_size : integer;
                   files     : array of TFile;
                   dirs      : array of TDirectory;
                   parent    : PDirectory;
                 end;

procedure dump_tree_(d : PDirectory; prefix:string);
var i : integer;
begin
   for i := low(d^.files) to high(d^.files) do
      writeln(prefix, d^.files[i].name,' [', d^.files[i].size, ']');
   for i := low(d^.dirs) to high(d^.dirs) do
   begin
      writeln(prefix, d^.dirs[i].name, ' [', d^.dirs[i].file_size, '] [', d^.dirs[i].all_size, ']');
      dump_tree_(@d^.dirs[i], prefix + '  ');
   end;
end;

procedure dump_tree(d : PDirectory; prefix:string);
var i : integer;
begin
   writeln(prefix, d^.name, ' [', d^.file_size, ']');
   dump_tree_(d, prefix + '  ');
end;

var
   fd_in    : TextFile;
   line     : string;
   root     : TDirectory;
   cur      : ^TDirectory;
   i        : integer;
   s        : string;
   bits     : TStringDynArray;
   work_dir : TDirectory;

function find_dir(d : string) : PDirectory;
var i : integer;
begin
   for i := low(cur^.dirs) to high(cur^.dirs) do
   begin
      writeln(d, ' =? ', cur^.dirs[i].name);
      if d = cur^.dirs[i].name then
      begin
         find_dir := @cur^.dirs[i];
         exit;
      end;
   end;
   find_dir := nil;
end;

procedure add_dir(d : string);
var dir : TDirectory;
begin
   if find_dir(d) <> nil then exit;
   dir.name := d;
   dir.parent := cur;
   dir.file_size := 0;
   dir.all_size := 0;
   Insert(dir, cur^.dirs, high(cur^.dirs)+1);
end;

procedure add_file(fn : string; fss : string);
var fs, code : integer;
   f         : TFile;
begin
   val(fss, fs, code);
   f.name := fn;
   writeln('file ', fn, ' in ', cur^.name);
   f.size := fs;
   cur^.file_size := cur^.file_size + fs;
   insert(f, cur^.files, high(cur^.files)+1);
end;

procedure update_sizes (d : PDirectory);
var i : integer;
   j  :  integer;
begin
   j := 0;
   for i := low(d^.dirs) to high(d^.dirs) do
   begin
      update_sizes(@d^.dirs[i]);
      j := j + d^.dirs[i].all_size;
   end;
   d^.all_size := j + d^.file_size;
end;

function calc_sum (d : PDirectory) : integer;
{
current all size < limit? - add to accumulator
check each child directory
}
var i  : integer;
   acc : integer;
begin
   calc_sum := 0;
   acc := 0;
   for i := low(d^.dirs) to high(d^.dirs) do
   begin
      { writeln('>', d^.dirs[i].name, ' ', acc, ' ', d^.dirs[i].all_size); }
      acc := acc + calc_sum(@d^.dirs[i]);
   end;
   if d^.all_size <= 100000 then
   begin
      { writeln(d^.name, ' ', acc, ' ', d^.all_size); }
      acc := acc + d^.all_size;
   end;
   calc_sum := acc;
end;

begin
   root.name := '/';
   root.parent := nil;
   cur := @root;
   AssignFile(fd_in, C_INPUT);
   reset(fd_in);
   while not eof(fd_in) do
   begin
      readln(fd_in, line);
      bits := SplitString(line, ' ');
      writeln('$$$ ', cur^.name, ' [', high(cur^.dirs), '] >>> ', line);
      if (bits[0] = 'dir') then { directory as part of a listing }
      begin
         add_dir(bits[1]);
      end;
      if pos(copy(bits[0], 1, 1), '0123456789') > 0 then
      begin
         add_file(bits[1], bits[0]);
      end;
      if (bits[0] = '$') and (bits[1] = 'cd') then
      begin
         { writeln(cur^.name, ' ', bits[2]); }
         if bits[2] = '/' then { change to root }
            cur := @root
         else if bits[2] = '..' then { change to parent directory }
            cur := cur^.parent
         else
         begin
            cur := find_dir(bits[2]);
         end;
      end;
   end;
   CloseFile(fd_in);

   update_sizes(@root);
   dump_tree(@root, '');

   writeln(calc_sum(@root));
end.
