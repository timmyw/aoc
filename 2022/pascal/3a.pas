{$mode objfpc}{$H+}
program aoc3a;

uses Sysutils, utils;

const
   C_INPUT   = '../input/input3';

function find_dup(l1, l2     : string): integer;
var d  : string;
   j,k : integer;
begin
   d := '';
   for j := 1 to length(l1) do
   begin
      k := pos(copy(l1, j, 1), l2);
      if k <> 0 then
      begin
         d := l2[k];
         break;
      end;
   end;

   find_dup := get_item_priority(d);

end;

var
   fd_in        : TextFile;
   line, l1, l2 : string;
   i, priority  : integer;

begin
   AssignFile(fd_in, C_INPUT);
   reset(fd_in);
    priority := 0;

   while not eof(fd_in) do
   begin
      readln(fd_in, line);
      i := round(length(line)/2);
      l1 := copy(line, 1, i);
      l2 := copy(line, i+1, i);
      writeln(l1);
      writeln(l2);
      priority += find_dup(l1, l2);
      writeln(priority);
   end;
   CloseFile(fd_in);
end.
