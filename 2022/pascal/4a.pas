{$mode objfpc}{$H+}
program aoc4a;

uses Sysutils, utils;

const
   C_INPUT   = '../input/input4';

type range = record
                s : integer;
                e : integer;
end;

function range_to_str(r : range):string;
begin
   range_to_str := format('%2d-%2d', [r.s, r.e]);
end;

function extract_range(s: string): range;
var
   r      : range;
   i,code : integer;
begin
   i := pos('-', s);
   if i = 0 then exit;
   val(copy(s, 1, i-1), r.s, code);
   val(copy(s, i+1, length(s)), r.e, code);
   extract_range := r;
end;

function over_lap(r1, r2 : range) : boolean;
begin
   over_lap := False;
   if (r1.s <= r2.s) and (r1.e >= r2.e) then over_lap := True;
end;

var
   fd_in    : TextFile;
   line     : string;
   count, i : integer;
   r1, r2   : range;
begin
   AssignFile(fd_in, C_INPUT);
   reset(fd_in);
   count := 0;

   while not eof(fd_in) do
   begin
      readln(fd_in, line);
      i := pos(',', line);
      r1 := extract_range(copy(line, 1, i-1));
      r2 := extract_range(copy(line, i+1, length(line)));
      if (over_lap(r1, r2)) or (over_lap(r2, r1)) then count := count + 1;
      { write('[', format('%-20s', [line]),'] '); }
      { if over_lap(r1, r2) then }
      { begin }
      {    count := count + 1; }
      {    write(range_to_str(r1), '<--', range_to_str(r2)); }
      { end; }
      { if over_lap(r2, r1) then }
      { begin }
      {    count := count + 1; }
      {    write(range_to_str(r2), '<--', range_to_str(r1)); }
      { end; }
      { writeln(); }
   end;
   CloseFile(fd_in);
   writeln('Overlaps:', count);
end.
