{$mode objfpc}{$H+}
program aoc4a;

uses Sysutils, utils;

const
   C_INPUT   = '../input/input4';

function overlap(r1, r2 :  range): boolean;
begin
   overlap := False;
   if (r1.s <= r2.s) and (r1.e >= r2.s) then overlap := True;
   if (r1.e >= r2.e) and (r1.s <= r2.e) then overlap := True;
   if (r2.s <= r1.s) and (r2.e >= r1.s) then overlap := True;
   if (r2.e >= r1.e) and (r2.s <= r1.e) then overlap := True;
end;

var
   fd_in     : TextFile;
   line      : string;
   count, i  : integer;
   r1, r2, r : range;
begin
   AssignFile(fd_in, C_INPUT);
   reset(fd_in);
   count := 0;

   while not eof(fd_in) do
   begin
      readln(fd_in, line);
      i := pos(',', line);
      r1 := extract_range(copy(line, 1, i-1));
      r2 := extract_range(copy(line, i+1, length(line)));
      r := merge_ranges(r1, r2);
      { if (overlap(r, r1)) then count := count + 1; }

      write('[', format('%-20s', [line]),'] [', range_to_str(r), '] ');
      if overlap(r1, r2) then
      begin
         count := count + 1;
         write(range_to_str(r1), '<--', range_to_str(r2));
      end;
      writeln();

   end;
   CloseFile(fd_in);
   writeln('Overlaps:', count);
end.
