{$mode objfpc}{$H+}
program aoc8a;

uses Sysutils, utils, Types, StrUtils;

const
   C_INPUT    = '../input/input8';

var
   fd_in        : TextFile;
   line         : string;
   lines        : array of string;
   i, j, count  : integer;
   width, depth : integer;

function tallest(ci, cj, ii, jj : integer ) : boolean;
var c                               : string;
   s                                : string;
begin
   tallest := False;
   c := copy(lines[ci], cj+1, 1);
   writeln(ci, ':', cj, ' >', c);
   while (ci > 0) and (ci < depth) and (cj > 0) and (cj < width) do
   begin
      ci := ci + ii;
      cj := cj + jj;
      s := copy(lines[ci], cj+1, 1);
      writeln(ci, ':', cj, ' :', s, '>=', c);
      if s >= c then
         exit;
   end;
   tallest := True;
end;

begin
   count := 0;
   AssignFile(fd_in, C_INPUT);
   reset(fd_in);
   while not eof(fd_in) do
   begin
      readln(fd_in, line);
      insert(line, lines, high(lines)+1);
   end;
   CloseFile(fd_in);

   width := length(lines[0])-1;
   depth := high(lines);
   for i := 0 to depth do
      for j := 0 to width do
      begin
         if (i = 0) or (i = depth) or (j = 0) or (j = width) then
         begin
            count := count + 1;
         end
         else
         begin
            if tallest(i, j, 1, 0) or
               tallest(i, j, -1, 0) or
               tallest(i, j, 0, 1) or
               tallest(i, j, 0, -1)
               then count := count + 1;
         end;
      end;

   writeln(count);
end.
