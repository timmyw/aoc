{$mode objfpc}{$H+}
program aoc1;

uses
Sysutils;

const
   C_INPUT   = '../input/input1';
   max_count =  1000;

type
   count_array = array [0..max_count] of integer;

var
   fd_in  : TextFile;
   line   : string;
   counts : count_array;
   i,j,n  : integer;
   max    : array [1..3] of integer;

procedure init_counts;
var k : integer;
begin
   for k := 0 to max_count do
      counts[k] := 0;
   for k := 1 to 3 do
      max[k] := 0;
end;

procedure check_new_max(o : integer);
var t,s :  integer;
begin
   for s := 1 to 3 do
   begin
      if o > max[s] then
      begin
         t := max[s];
         max[s] := o;
         check_new_max(t);
         exit;
      end;
   end;
end;

begin
   AssignFile(fd_in, C_INPUT);
   reset(fd_in);
   init_counts();
   i := 0;
   n := 0;

   while not eof(fd_in) do
   begin
      readln(fd_in, line);
      n := n + 1;
      if line = '' then
         begin
         i := i + 1;
         counts[i] := 0
         end
      else
         counts[i] := counts[i] + StrToInt(line);

      { Check for a new max }
      check_new_max(counts[i]);
   end;
   CloseFile(fd_in);

   for j := 1 to 3 do
      writeln(max[j]);
   writeln('Max 3 total ', max[1] + max[2] + max[3]);
   writeln(n);

   { 69092
69625
72240 }
   { for j := 0 to 250 do }
   { begin }
   {    writeln(counts[j]); }
   { end; }
end.
