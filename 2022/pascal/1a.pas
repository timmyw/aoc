{$mode objfpc}{$H+}
program aoc1;

uses
Sysutils;

const
   C_INPUT = '../input/input1';

type
   count_array = array [0..1000] of integer;

var
   fd_in     : TextFile;
   line      : string;
   counts    : count_array;
   i, j, max : integer;

begin
   AssignFile(fd_in, C_INPUT);
   reset(fd_in);
   i := 0;
   max := 0;
   while not eof(fd_in) do
   begin
      readln(fd_in, line);
      if line = '' then
         begin
         i := i + 1;
         counts[i] := 0
         end
      else
         counts[i] := counts[i] + StrToInt(line);
      if counts[i] > max then
         begin
            max := counts[i];
            j := i;
         end;
   end;
   CloseFile(fd_in);

   writeln('Elf ', j+1, ' with ', max);

   { for j := 0 to i do }
   {    begin }
   {       writeln(j, '=', counts[j]); }
   {    end; }

end.
