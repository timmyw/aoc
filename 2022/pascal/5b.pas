{$mode objfpc}{$H+}
program aoc5a;

uses Sysutils, utils, Types, StrUtils;

var stacks : array [1..9] of string = (
                                       'RNFVLJSM',
                                       'PNDZFJWH',
                                       'WRCDG',
                                       'NBS',
                                       'MZWPCBFN',
                                       'PRMW',
                                       'RTNGLSW',
                                       'QTHFNBV',
                                       'LMHZNF'
                                      );
const
   C_INPUT   = '../input/input5';

{
[M] [H]         [N]
[S] [W]         [F]     [W] [V]
[J] [J]         [B]     [S] [B] [F]
[L] [F] [G]     [C]     [L] [N] [N]
[V] [Z] [D]     [P] [W] [G] [F] [Z]
[F] [D] [C] [S] [W] [M] [N] [H] [H]
[N] [N] [R] [B] [Z] [R] [T] [T] [M]
[R] [P] [W] [N] [M] [P] [R] [Q] [L]
 1   2   3   4   5   6   7   8   9
}

procedure dump_stacks();
var i, j, k : integer;
   s        : string;
begin
   j := 0;
   for i := 1 to 9 do
      if length(stacks[i]) > j then j := length(stacks[i]);
   for k := j downto 1 do
   begin
      for i := 1 to 9 do
      begin
         s := copy(stacks[i], k, 1);
         if s <> '' then
            write('[', s, '] ')
         else
            write('    ');
      end;
      writeln();
   end;
end;

procedure get_move(      l : string;
                   out cnt : integer;
                   out frm : integer;
                   out dst : integer);
var bits : TStringDynArray;
    code : integer;
   i     : integer;
begin
   { move 1 from 7 to 6 }
   bits := SplitString(l, ' ');
   val(bits[1], cnt, code);
   val(bits[3], frm, code);
   val(bits[5], dst, code);
end;

procedure apply_move(l : string);
var
   cnt, frm, dst : integer;
   i             : integer;
   t             : string;
begin
   get_move(l, cnt, frm, dst);
   for i:= 1 to cnt do
   begin
      t := copy(stacks[frm], length(stacks[frm]), 1);
      stacks[frm] := copy(stacks[frm], 1, length(stacks[frm])-1);
      stacks[dst] := stacks[dst] + t;
   end;
end;

var
   fd_in     : TextFile;
   line      : string;
   count, i  : integer;
begin
   AssignFile(fd_in, C_INPUT);
   reset(fd_in);

   while not eof(fd_in) do
   begin
      readln(fd_in, line);
      { dump_stacks(); }
      { exit(); }
      apply_move(line);
   end;
   CloseFile(fd_in);
   writeln('Final tops:');
   for i := 1 to 9 do
      write(copy(stacks[i], length(stacks[i]), 1));
   writeln();
end.
