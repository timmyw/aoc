{$mode objfpc}{$H+}
program aoc8b;

uses Sysutils, utils, Types, StrUtils;

const
   C_INPUT    = '../input/input8';

var
   fd_in             : TextFile;
   line              : string;
   lines             : array of string;
   i, j, scenic, tmp : integer;
   width, depth      : integer;

function get_tree(ci, cj :  integer) : string;
begin
   get_tree := copy(lines[ci-1], cj, 1);
end;

function get_scenic_score_dir(ci, cj, ii, jj: integer) : integer;
var cnt : integer;
   c, s : string;
begin
   cnt := 0;
   c := get_tree(ci, cj);
   writeln(ci, ':', cj, ' :', c);
   { while (ci >= 0) and (ci <= depth) and (cj >= 0) and (cj <= width) do }
   while True do
   begin
      ci := ci + ii;
      cj := cj + jj;
      if (ci < 1) or (cj < 1) or (ci > 99) or (cj > 99) then
         s := 'Z'
      else
      begin
         s := get_tree(ci, cj);
         cnt := cnt + 1;
      end;
      writeln(ci, ':', cj, ' [', ii, ',', jj, ']   :', s, '>', c, ' --', cnt);
      if s >= c then
         break;
   end;
   writeln();
   get_scenic_score_dir := cnt;
end;

function get_scenic_score (ci, cj            : integer) : integer;
begin
   if (ci = 1) or (cj = 1) or (ci = 99) or (cj = 99) then
   begin
      get_scenic_score := 0;
      exit;
   end;
   get_scenic_score := get_scenic_score_dir(ci, cj, 1, 0) *
   get_scenic_score_dir(ci, cj, -1, 0) *
   get_scenic_score_dir(ci, cj, 0, 1) *
   get_scenic_score_dir(ci, cj, 0, -1);
end;

begin
   scenic := 0;
   AssignFile(fd_in, C_INPUT);
   reset(fd_in);
   while not eof(fd_in) do
   begin
      readln(fd_in, line);
      insert(line, lines, high(lines)+1);
   end;
   CloseFile(fd_in);

   depth := high(lines)-low(lines);
   width := length(lines[0]);

   for i := 1 to 99 do
      for j := 1 to 99 do
      begin
         tmp := get_scenic_score(i,j);
         if tmp > scenic then scenic := tmp;
      end;

   writeln(scenic);

end.

{
5764801
}
