{$mode objfpc}{$H+}
program aoc9b;

uses Sysutils, utils, Types, StrUtils;

const
   C_INPUT    = '../input/input9';
   width      = 500;
   depth      = 500;
   tail_count = 10;

type
   TPos =  record
              x, y : integer;
           end;
var
   tx, ty : array [1..tail_count] of integer;
   track  : array [1..width,1..depth] of integer;

function count_visits () : integer;
var cnt : integer;
   i, j : integer;
begin
   cnt := 0;
   for i := 1 to width do
      for j :=1 to depth do
         if track[i,j] > 0 then
            cnt := cnt + 1;
   count_visits := cnt;
end;

procedure move_tail (k : integer );
var otx, oty           : integer;
begin
   if k > tail_count then exit;
   if (abs(tx[k]-tx[k-1]) > 1) or (abs(ty[k]-ty[k-1]) > 1) then
   begin
      { if abs(hy-ty) <= 1 then exit; }
      if k = tail_count then
         otx := tx[k]; oty := ty[k];
      if tx[k] < tx[k-1] then tx[k] := tx[k] + 1;
      if tx[k] > tx[k-1] then tx[k] := tx[k] - 1;
      if ty[k] < ty[k-1] then ty[k] := ty[k] + 1;
      if ty[k] > ty[k-1] then ty[k] := ty[k] - 1;
      if (k = tail_count) and ((otx <> tx[k]) or (oty <> ty[k])) then { only for final tail }
         track[tx[k], ty[k]] := track[tx[k], ty[k]] + 1;
   end;
   move_tail(k+1);
end;

procedure move_head (cnt, xi, yi : integer);
begin
   while cnt > 0 do
   begin
      tx[1] := tx[1] + xi;
      ty[1] := ty[1] + yi;
      { writeln(' head: [', tx[1], ',', ty[1], ']'); }
      move_tail(2);
      cnt := cnt - 1;
   end;
end;

procedure apply_move (l : string);
var
   bits      : TStringDynArray;
   cnt, code : integer;
begin
   bits := splitstring(l, ' ');
   val(bits[1], cnt, code);
   if bits[0] = 'R' then move_head(cnt, 1, 0);
   if bits[0] = 'L' then move_head(cnt, -1, 0);
   if bits[0] = 'U' then move_head(cnt, 0, -1);
   if bits[0] = 'D' then move_head(cnt, 0, 1);
end;

procedure init();
var i, j : integer;
begin
   for i := 1 to tail_count do
   begin
      tx[i] := trunc(width / 2);
      ty[i] := trunc(depth / 2);
   end;

   for i := 1 to width do
      for j := 1 to depth do
         track[i,j] := 0;
   track[tx[tail_count], ty[tail_count]] := 1;
end;

var
   fd_in          : TextFile;
   line           : string;
begin
   init();
   AssignFile(fd_in, C_INPUT);
   reset(fd_in);
   while not eof(fd_in) do
   begin
      readln(fd_in, line);
      apply_move(line);
   end;
   CloseFile(fd_in);

   writeln('Visits: ', count_visits());

end.
