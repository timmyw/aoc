{$mode objfpc}{$H+}
program aoc9a;

uses Sysutils, utils, Types, StrUtils;

const
   C_INPUT = '../input/input9';
   width   = 500;
   depth   = 500;
type
   TPos =  record
              x, y : integer;
           end;
var
   hx, hy, tx, ty : integer;
   track          : array [1..width,1..depth] of integer;

function count_visits () : integer;
var cnt : integer;
   i, j : integer;
begin
   cnt := 0;
   for i := 1 to width do
      for j :=1 to depth do
         if track[i,j] > 0 then
            cnt := cnt + 1;
   count_visits := cnt;
end;

procedure move_tail ();
var otx, oty : integer;
begin
   writeln('x:', abs(hx-tx), ' y:', abs(hy-ty));
   if (abs(hx-tx) <= 1) and (abs(hy-ty) <= 1) then exit;
   { if abs(hy-ty) <= 1 then exit; }
   otx := tx; oty := ty;
   if tx < hx then tx := tx + 1;
   if tx > hx then tx := tx - 1;
   if ty < hy then ty := ty + 1;
   if ty > hy then ty := ty - 1;
   if (otx <> tx) or (oty <> ty) then
      track[tx, ty] := track[tx, ty] + 1;
end;

procedure move_head (cnt, xi, yi : integer);
begin
   while cnt > 0 do
   begin
      hx := hx + xi;
      hy := hy + yi;
      writeln(' head: [', hx, ',', hy, '] [', tx, ',', ty, ']');
      move_tail();
      cnt := cnt - 1;
   end;
end;

procedure apply_move (l : string);
var
   bits      : TStringDynArray;
   cnt, code : integer;
begin
   bits := splitstring(l, ' ');
   val(bits[1], cnt, code);
   if bits[0] = 'R' then move_head(cnt, 1, 0);
   if bits[0] = 'L' then move_head(cnt, -1, 0);
   if bits[0] = 'U' then move_head(cnt, 0, -1);
   if bits[0] = 'D' then move_head(cnt, 0, 1);
end;

procedure init();
var i, j : integer;
begin
   hx := trunc(width / 2); hy := trunc(depth / 2);
   tx := hx; ty := hy;
   for i := 1 to width do
      for j := 1 to depth do
         track[i,j] := 0;
   track[tx, ty] := 1;
end;

var
   fd_in          : TextFile;
   line           : string;
begin
   init();
   AssignFile(fd_in, C_INPUT);
   reset(fd_in);
   while not eof(fd_in) do
   begin
      readln(fd_in, line);
      apply_move(line);
   end;
   CloseFile(fd_in);

   writeln('Visits: ', count_visits());

end.
