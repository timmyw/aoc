unit data11;

interface

type TMonkey = record
                  num         : integer;
                  items       : array of integer;
                  op          : string;
                  operand     : string;
                  test_div    : integer;
                  true_route  : integer;
                  false_route : integer;
               end;

type PMonkey = ^TMonkey;
type TMonkeys = array of TMonkey;
type PMonkeys = ^TMonkeys;

var
   monkeys      : array of TMonkey;
   test_monkeys : array [0..3] of TMonkey =
   (
    ( num : 0; items : ( 79, 98 ); op : '*'; operand : '19'; test_div : 23; true_route : 2; false_route : 3; ),
    ( num : 1; items : ( 54, 65,75, 74 ); op : '+'; operand : '6'; test_div : 19; true_route : 2; false_route : 0; ),
    ( num : 2; items : ( 79, 60, 97 ); op : '*'; operand : 'old'; test_div : 13; true_route : 1; false_route : 3; ),
    ( num : 3; items : ( 74 ); op : '+'; operand : '3'; test_div : 17; true_route : 0; false_route : 1; )
    );

implementation

end.
