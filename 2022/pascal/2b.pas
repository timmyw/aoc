{$mode objfpc}{$H+}
program aoc1;

uses
Sysutils;

type
   Outcome = record
                t : string; {their play }
                m : string; { my play }
                p : integer; { score from playing mine }
                r : integer; { round score }
             end;

const
   C_INPUT                            = '../input/input2';
   max_count                          = 1000;

   outcomes : array [0..8] of Outcome =
   (
    (t: 'A'; m: 'X'; p: 1; r: 3), { Rock vs Rock}
    (t: 'A'; m: 'Y'; p: 2; r: 6), { Rock vs Paper}
    (t: 'A'; m: 'Z'; p: 3; r: 0), { Rock vs Scissors}
    (t: 'B'; m: 'X'; p: 1; r: 0), { Paper vs Rock }
    (t: 'B'; m: 'Y'; p: 2; r: 3), { Paper vs Paper}
    (t: 'B'; m: 'Z'; p: 3; r: 6), { Paper vs Scissors }
    (t: 'C'; m: 'X'; p: 1; r: 6), { Scissors vs Rock }
    (t: 'C'; m: 'Y'; p: 2; r: 0), { Scissors vs Paper }
    (t: 'C'; m: 'Z'; p: 3; r: 3)  { Scissors vs Scissors }
    );

var
   fd_in        : TextFile;
   line         : string;
   score        : integer;
   theirs, mine : string;

function round_score(t : string; m : string) : integer;
var i, round : integer;
begin
   round := 0;
   if m = 'Y' then round := 3;
   if m = 'Z' then round := 6;
   for i := 0 to 8 do
   begin
      if (outcomes[i].r = round) and (outcomes[i].t = t) then
         round_score := round + outcomes[i].p;
   end;
end;

begin
   assignfile(fd_in, C_INPUT);
   reset(fd_in);
   score := 0;

   while not eof(fd_in) do
   begin
      readln(fd_in, line);
      theirs := copy(line, 1, 1);
      mine := copy(line, 3, 1);
      score := score + round_score(theirs, mine);
      writeln(theirs, ' <> ', mine, ' round=', round_score(theirs, mine), ' score=', score);
   end;
   CloseFile(fd_in);
   writeln('Total score=', score)
end.
