unit utils;

interface

function get_item_priority(d : string) : integer;
function exist_in_string(haystack, needle : string) :boolean;

function max(i,j :  integer) : integer;
function min(i,j :  integer) : integer;

type range = record
                s : integer;
                e : integer;
end;

function over_lap(r1, r2 : range) : boolean;
function merge_ranges(r1, r2 :  range) : range;
function range_to_str(r : range):string;
function extract_range(s: string): range;

implementation

uses Sysutils;

function get_item_priority(d : string) : integer;
begin
   if d >= 'a' then
      get_item_priority := ord(d[1])-96
   else
      get_item_priority := ord(d[1])-64+26
end;

function exist_in_string(haystack, needle : string) :boolean;
begin
   if pos(needle, haystack) > 0 then
      exist_in_string := True
   else
      exist_in_string := False
end;

function max(i,j :  integer) : integer;
begin
   if i >= j then
      max := i
   else
      max := j;
end;

function min(i,j :  integer) : integer;
begin
   if i <= j then
      min := i
   else
      min := j;
end;

{ ranges }
function over_lap(r1, r2 : range) : boolean;
begin
   over_lap := False;
   if (r1.s <= r2.s) and (r1.e >= r2.e) then over_lap := True;
end;

function merge_ranges(r1, r2 :  range) : range;
var r :  range;
begin
   r.s := min(r1.s, r2.s);
   r.e := max(r1.e, r2.e);
   merge_ranges := r;
end;

function range_to_str(r : range):string;
begin
   range_to_str := format('%2d-%2d', [r.s, r.e]);
end;

function extract_range(s: string): range;
var
   r      : range;
   i,code : integer;
begin
   i := pos('-', s);
   if i = 0 then exit;
   val(copy(s, 1, i-1), r.s, code);
   val(copy(s, i+1, length(s)), r.e, code);
   extract_range := r;
end;

end.
