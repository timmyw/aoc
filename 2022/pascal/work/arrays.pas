{$mode objfpc}{$H+}
program arrays;

uses Sysutils, Types, StrUtils;

type
   ddd =  record
             n : string;
             v : integer;
          end;

procedure print_ddd(d : ddd);
begin
   write(d.n, '=', d.v);
end;

var
   ii : array of ddd;
   j  : integer;
   d  : ddd;
   dp : ^ddd;
begin
   d.n := 'one';
   d.v := 99;
   insert(d, ii, high(ii));
   d.n := 'two';
   d.v := 7;
   insert(d, ii, high(ii)+1);

   dp := @ii[high(ii)];
   writeln(dp^.n);

   for j := low(ii) to high(ii) do
   begin
      print_ddd(ii[j]);
      write(', ');
   end;
   writeln();
end.
