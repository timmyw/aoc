{$mode objfpc}{$H+}
program aoc1;

uses
Sysutils;

const
   C_INPUT   = '../input/input2';
   max_count =  1000;

var
   fd_in        : TextFile;
   line         : string;
   score        : integer;
   theirs, mine : string;

function winner(t : string; m: string ) : integer;
var score : integer;
begin
   score := 0;
   if ((t = 'A') and (m = 'X'))
      or ((t = 'B') and (m = 'Y'))
      or ((t = 'C') and (m = 'Z')) then
      score := 3;
   if ((m = 'X') and (t = 'C'))
      or ((m = 'Y') and (t = 'A'))
      or ((m = 'Z') and (t = 'B')) then
      score := 6;
   winner := score;
end;

function round_score(t: string; m : string ) : integer;
var score : integer;
begin
   score := winner(t, m);
   if m = 'X' then
      score := score + 1;
   if m = 'Y' then
      score := score + 2;
   if m = 'Z' then
      score := score + 3;
   writeln('__ ', t, ' <> ', m, ' ', score);
   round_score := score;
end;

begin
   AssignFile(fd_in, C_INPUT);
   reset(fd_in);
   score := 0;

   while not eof(fd_in) do
   begin
      readln(fd_in, line);
      theirs := copy(line, 1, 1);
      mine := copy(line, 3, 1);
      score := score + round_score(theirs, mine);
      writeln(theirs, ' <> ', mine, ' round=', round_score(theirs, mine), ' score=', score);
   end;
   CloseFile(fd_in);
   writeln('Total score=', score)
end.
