# aoc

Central point for all my AoC attempts


[2024](./2024)

[2023](./2023)

[2022 (pascal)](./2022/pascal)

[(part of) 2021](./2021/README.md)
