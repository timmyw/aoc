/*
 * day1a
 */

#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include <filesystem>
#include <iostream>
#include <cstdlib>
#include <algorithm>

std::string input = "../../input/test_1";

int split_string(std::string inp, std::vector<std::string> &out, char delim) {
  std::istringstream ss(inp);
  std::string s;
  int i = 0;
  while (std::getline(ss, s, delim)) {
    if (s.length()) {
      i++;
      out.push_back(s);
    }
  }
  // std::cout << "split_string:" << i << ":" << out.size() << std::endl;
  return out.size();
}

std::vector<std::string> load_input(std::string& filename) {
  std::vector<std::string> lines;
  std::ifstream f(filename,  std::ios::in);
  if (!f.is_open())
    return lines;

  const auto sz = std::filesystem::file_size(filename);
  std::string result(sz, '\0');
  f.read(result.data(), sz);

  auto c = split_string(result, lines, '\n');
  // std::cout << "lines:" << c << std::endl;

  return lines;
}

int split_strings(std::vector<std::string> inp, std::vector<int> &left, std::vector<int> &right) {
  for (auto i : inp) {
    std::vector<std::string> nums;
    split_string(i, nums, ' ');
    left.push_back(atoi(nums[0].c_str()));
    right.push_back(atoi(nums[1].c_str()));
  }
  return left.size();
}

int main(int argc, char* argv[])
{
  auto lines = load_input(input);
  std::vector<int> left;
  std::vector<int> right;

  split_strings(lines, left, right);
  // std::cout << left.size() << ":" << right.size() << std::endl;
  std::sort(left.begin(), left.end(), [](int a, int b) { return a <= b; });
  std::sort(right.begin(), right.end(), [](int a, int b) { return a <= b; });

  int distances = 0;
  for (int i = 0; i < left.size(); i++) { //
    auto d = abs(left[i] - right[i]);
    // std::cout << left[i] << ":" << right[i] << " " << d << "\n" << std::endl;
    distances += d;
  }
  std::cout << "Sum of distances:" << distances << std::endl;
  return 0;
}
