/*
 * day10a
 */

#include <vector>
#include <string>
#include <iostream>
#include <utility>
#include <boost/format.hpp>

#include "strutils.h"
#include "board.h"

auto input = load_board(get_input_path() + "/10");

/*
  Start from supplied start
  Look up, right, down, left
 */
int trailhead_scores(int r, int c, board& m) {
  std::cout << r << "," << c << " = " << m.get_char(r, c) << std::endl;
  char ch = m.get_char(r, c);
  if (ch == 'X')
    return 0;			// We've visited this one before
  if (ch == '9') {
    m.set_char(r, c, 'X');
    return 1;			// End case - we successfully got to '9'
  }
  int s = 0;
  ch++;
  if (m.get_char(r-1, c) == ch)
    s += trailhead_scores(r-1, c, m);
  if (m.get_char(r,c-1) == ch)
    s += trailhead_scores(r, c-1, m);
  if (m.get_char(r+1,c) == ch)
    s += trailhead_scores(r+1, c, m);
  if (m.get_char(r,c+1) == ch)
    s += trailhead_scores(r, c+1, m);
  return s;
}

int main(int argc, char* argv[])
{
  int sum = 0;
  input.dump();
  for (int r = 0; r < input.rows; r++)
    for (int c = 0; c < input.cols; c++) {
      if (input.get_char(r, c) == '0') {	// We have a trailhead
	auto m = input;
	std::cout << "start:" << r << "," << c << " = " << input.get_char(r, c)	<< std::endl;
	sum += trailhead_scores(r, c, m);
	// return 0;
      }
    }
  std::cout << "Sum: " << sum << std::endl;

  return 0;
}

/*
  Cycle through the board
  For each antenna
    Scan outward and find each occurence of same antenna
      For new occurence set an antinode (if it is in board coords)
  Count all antinodes
 */
