/*
 * day11a
 */

#include <algorithm>
#include <string>
#include <iostream>
#include <vector>
#include <boost/format.hpp>

#include "io.h"
#include "strutils.h"

std::string inp = load_input_whole(get_input_path() + "/11");

typedef int64_t inttype;
typedef std::vector<inttype> inttype_v;

typedef std::vector<std::string> string_v;

template<typename T>
void dump_v(const T& v) {
  for (auto i : v) {
    std::cout << i << " ";
  }
  std::cout << std::endl;
}

void convert_stones(const std::string& inp, inttype_v& m) {
  std::vector<std::string> vs;
  split_string(inp, vs, ' ');
  m.resize(vs.size());
  std::transform(vs.begin(),
		 vs.end(),
		 m.begin(),
		 [](const std::string& s) { return std::stoll(s); });
}

void split_digits(const std::string s, std::string& left, std::string& right) {
  auto l = s.size() / 2;
  left = s.substr(0, l);
  right = s.substr(l, s.size()-l);
}

std::string to_int_and_back(const std::string& s) {
  inttype x = std::stoll(s);
  return str(boost::format("%ld") % x);
}

/* from begin - pick up an integer
 apply rules - either update or split (after split jump over both new ints)
 move onto next int
*/
void blink(string_v& m) {
  string_v mn;
  for (string_v::iterator i = m.begin(); i != m.end(); i++) {
    std::string j = *i;
    // std::cout << "$$$ " << j << " ";
    if (j == "0") {
      mn.push_back("1");
      // std::cout << "setting to 1" << std::endl;
    } else if (j.length() % 2 == 0) {
      std::string l, r;
      split_digits(j, l, r);
      mn.push_back(to_int_and_back(l));
      mn.push_back(to_int_and_back(r));
    } else {
      inttype x = std::stoll(j) * 2024;
      std::string s = str(boost::format("%ld") % x);
      mn.push_back(s);
    }
  }
  m = mn;
}

int main(int argc, char* argv[])
{
  // inttype_v stones;
  // convert_stones(inp, stones);
  string_v stones;
  split_string(inp, stones, ' ');
  dump_v(stones);
  for (int i = 1; i <= 25; i++) {
    blink(stones);
    // std::cout << "BLINK:" << i << " " << stones.size() << " ";
    // dump_v(stones);
  }

  std::cout << "length:" << stones.size() << std::endl;
  return 0;
}

/*
 */
