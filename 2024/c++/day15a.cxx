/*
 * day15a
 */

#include <iostream>
#include <boost/format.hpp>
#include <curses.h>

#include "strutils.h"
#include "io.h"
#include "board.h"

auto input_s = load_input_whole(get_input_path() + "/15");

typedef int int_t;

board b;
std::string moves;

coord find_robot() {
  return b.find(0, 0, '@');
}

coord
translate_dir(char ch) {
  switch (ch) {
  case '>': return coord{0, 1};
  case 'V':
  case 'v':return coord{1, 0};
  case '<': return coord{0, -1};
  case '^': return coord{-1, 0};
  }
  return coord{0, 0};
}

/*
  if '#' - return start
  if 'O' then call myself again
  if we can move, replace the original and set the new position.
  then return
 */
void
move_char(coord start, coord& dir, char dir_ch, int iter, int iter_max) {
  // std::cout << "MOVE:" << start << " - " << dir << std::endl;
  char org = b.get_char(start);	// Whatever we are trying to move
  mvaddstr(0, 0, (str(boost::format("direction: %c : %c {%d, %d} %d/%d") % org % dir_ch % dir.r % dir.c % iter % iter_max)).c_str());
  coord tmp = start + dir;	// Where we intend to move
  auto ch = b.get_char(tmp);	// What's in the way
  if (ch == '#')		// Wall - so stop trying
    return;
  if (ch == 'O')		// Move the barrel first
    move_char(tmp, dir, dir_ch, iter, iter_max);
  ch = b.get_char(tmp);		// Once the move has happened, what is now in the
				// way?
  // std::cout << "potential:" << tmp << ":" << ch << std::endl;
  if (ch == '.') {
    b.set_char(start, '.');
    start = tmp;
    // std::cout << start << " -> " << org << std::endl;
    b.set_char(start, org);
  }
}

void dump_board(coord& cur, coord& dir) {
  for (int r = 0; r < b.rows; r++) {
    mvaddstr(3+r, 0, b.lines[r].c_str());
  }

  attron(COLOR_PAIR(1));
  mvaddch(3+cur.r, cur.c, b.get_char(cur));
  attroff(COLOR_PAIR(1));

}

int
calculate_gps() {
  int_t gpssum = 0;
  for (int r = 0; r < b.rows; r++)
    for (int c = 0; c < b.rows; c++) {
      auto ch = b.get_char(r, c);
      if (ch == 'O')
	gpssum += r * 100 + c;
    }
  return gpssum;
}

int main(int argc, char* argv[])
{
  std::vector<std::string> input;
  split_string(input_s, input, '\n', false);
  bool board_or_moves = true;
  for (auto s : input) {
    if (s == "")
      board_or_moves = false;
    else if (board_or_moves)
      b.add_row(s);
    else
      moves += s;
  }

  initscr();
  cbreak();
  noecho();
  start_color();
  init_pair(1, COLOR_WHITE, COLOR_RED);

  int iter = 0;
  for (auto ch : moves) {
    coord robot = find_robot();
    coord dir = translate_dir(ch);
    dump_board(robot, dir);
    move_char(robot, dir, ch, iter, moves.length());
    iter++;
    //    getch();
  }

  endwin();

  b.dump();
  int gps_sum = calculate_gps();
  std::cout << "GPS sum:" << gps_sum << std::endl;

  return 0;
}

/*
*/
