/*
 * day16a
 */

#include <iostream>
#include <boost/format.hpp>
#include <queue>
#include <limits>

#include "strutils.h"
#include "io.h"
#include "board.h"
#include "direction.h"

typedef long unsigned int_t;

board b = load_board(get_input_path() + "/test_16");

coord direction = DIRECTIONS[0]; // East
coord current = b.find('S', 0, 0);
coord finish = b.find('E', 0, 0);
int_t cost = 0;

typedef struct {
  coord pos;
  int_t cost;

  // constexpr bool operator() (const position& p1, const position &p2) {
  //   return p1.cost < p2.cost;
  // }

} position;


#define INFINE_COST std::numeric_limits<int_t>::max()

typedef std::priority_queue<position> queue_t;


int main(int argc, char* argv[])
{
  std::priority_queue<position, std::vector<position>, std::greater<int_t>> Q;

  for (int r = 0; r < b.rows; r++)
    for (int c = 0; c < b.cols; c++) {
      Q.push(position{coord(r, c), INFINE_COST});
    }
  // while (b.get_char(current) != 'E') {
  //   auto new_cur = cur + direction;
  //   auto ch = b.get_char(new_cur);
  //   if (ch == '#') {
  //   }
  // }

  std::cout << "Cost:" << cost << std::endl;
  return 0;
}

/*
  -
*/
