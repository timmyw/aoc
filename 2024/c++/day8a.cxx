/*
 * day8a
 */

#include <vector>
#include <string>
#include <iostream>
#include <utility>
#include <boost/format.hpp>

#include "strutils.h"
#include "board.h"

auto input = load_board(get_input_path() + "/8");
board antinodes(input.cols, input.rows, 0);

void search_board(int o_r, int o_c, char cur_ant) {
  for (int r = 0; r < input.rows; r++)
    for (int c = 0; c < input.cols; c++) {
      char ch = input.get_char(r, c);
      if (ch == cur_ant && r != o_r && c != o_c) { // We've found a match
	int d_r = o_r - r, d_c = o_c - c;
	int n_r = o_r + d_r, n_c = o_c + d_c;
	if (antinodes.valid_coords(n_r, n_c)) {
	  char cur = antinodes.get_char(n_r, n_c);
	  if (cur == '.')
	    antinodes.set_char(n_r, n_c, 1);
	  else
	    antinodes.set_char(n_r, n_c, (int)cur + 1);
	}
      }
    }
}

int main(int argc, char* argv[])
{

  for (int r = 0; r < input.rows; r++)
    for (int c = 0; c < input.cols; c++) {
      char cur_ant = input.get_char(r, c);
      if (cur_ant != '.') {
	// Search the board again, but exclude the current coords.  Each further
	// antenna match - calc dir (delta_r, delta_c) and step out wards and create
	// an antinode
	search_board(r, c, cur_ant);
      }
    }

  int count = antinodes.count_not(0);
  std::cout << "Count: " << count << std::endl;

  return 0;
}

/*
  Cycle through the board
  For each antenna
    Scan outward and find each occurence of same antenna
      For new occurence set an antinode (if it is in board coords)
  Count all antinodes
 */
