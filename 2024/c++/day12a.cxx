/*
 * day12a
 */

#include <vector>
#include <string>
#include <iostream>
#include <utility>
#include <boost/format.hpp>

#include "strutils.h"
#include "board.h"

auto input = load_board(get_input_path() + "/12");

void build_working(board &main, board& working, int row, int col, char plant) {
  if (!main.valid_coords(row, col))
    return;
  if (main.get_char(row, col) == plant) {
    working.set_char(row, col, plant);
    main.set_char(row, col, tolower(plant));
    build_working(main, working, row-1, col, plant);
    build_working(main, working, row, col+1, plant);
    build_working(main, working, row+1, col, plant);
    build_working(main, working, row, col-1, plant);
  }
}

/*
  perimeter is any plant that borders a non plant, in any direction
 */
int count_perimeter(const board& b, char plant) {
  int cnt = 0;
  for (int r = 0; r < b.rows; r++)
    for (int c = 0; c < b.cols; c++) {
      auto ch = b.get_char(r, c);
      if (ch == plant) {
	// For each adjacent pos that isn't ~planet~, then increment cnt
	if (auto adj = b.get_char(r-1, c); adj != plant)
	  cnt++;
	if (auto adj = b.get_char(r, c+1); adj != plant)
	  cnt++;
	if (auto adj = b.get_char(r+1, c); adj != plant)
	  cnt++;
	if (auto adj = b.get_char(r, c+1); adj != plant)
	  cnt++;
      }
    }

  return cnt;
}

int main(int argc, char* argv[])
{
  int cost = 0;
  for (int r = 0; r < input.rows; r++)
    for (int c = 0; c < input.cols; c++) {
      char plant = input.get_char(r, c);
      if (std::islower(plant))	// We have already covered this
	continue;
      board working(input.rows, input.cols, 0);
      build_working(input, working, r, c, plant);
      input.dump(working);

      // Count the number of plants in working
      int count = working.count(plant);

      // Count the perimeter
      int perimeter = count_perimeter(working, plant);
      std::cout << "price: " << count << " * " << perimeter << " = " << count*perimeter << std::endl;
      std::cout << std::endl;

      cost += count * perimeter;
    }

  std::cout << "Cost:" << cost << std::endl;
  return 0;
}

/*
  Work through board
  For each untouched plant
  - create a working board
  - walk through the area for current plant - marking on the working board, and marking upper case on the main board.

    - Fan out recursively - use same boards.  If recursive call finds no same plants
      or already marked ones it returns

  - trace all edges of the working board (how?) to calculate perimeter and area
  -

 */
