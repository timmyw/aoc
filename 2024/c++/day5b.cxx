/*
 * day5b
 */

#include <algorithm>
#include <vector>
#include <string>
#include <iostream>
#include <utility>

#include "io.h"
#include "strutils.h"

std::string input = "5";
std::vector<std::string> lines;

typedef std::pair<int, int> rule;
typedef std::vector<rule> ruleset;

typedef std::vector<int> update;

inline bool is_rule(std::string s) {
  return s.find('|') != std::string::npos;
}

inline bool is_update(std::string s) {
  return s.find(',') != std::string::npos;
}

void add_rule_to_set(ruleset& rules, std::string l) {
  std::vector<std::string> vs;
  split_string(l, vs, '|');
  rules.push_back(rule(std::atoi(vs[0].c_str()), std::atoi(vs[1].c_str())));
  // std::cout << vs[0] << "|" << vs[1] << std::endl;
}

inline int get_middle(std::vector<int> vs) {
  return vs[vs.size() / 2];	// Zero based
}

void dump_update(update pages) {
  for (auto i : pages)
    std::cout << i << ",";
  std::cout << std::endl;
}

bool is_in_update(update pages, int p) {
  if (std::find(pages.begin(), pages.end(), p) != pages.end())
    return true;
  return false;
}

bool is_rule_appropriate(int p, rule &r, update pages) {
  return r.second == p && is_in_update(pages, r.first);
}

/* keep a "build" of pages
   for each page in the update:
   for each rule that applies to that page (second part) - has each (first part) page been printed?
 */
bool is_valid_update(const update& pages, const ruleset& rules) {
  update build;

  // std::cout << "UPDATE:"; dump_update(pages);
  for (auto p : pages) {
    for (auto r : rules) {
      if (is_rule_appropriate(p, r, pages)) {    // Only that specify
							      // what comes before
							      // the current page and
							      // have the first page
							      // in the update
							      // somewhere
	// std::cout << "PAGE:" << p << " rule " << r.first << "|" << r.second << std::endl;
	if (std::find(build.begin(), build.end(), r.first) == build.end())
	  return false;	      // We can't find the required page in the build buffer
      }
    }

    build.push_back(p);

  }
  // If we get here we haven't found any rules that invalid the update
  return true;
}

int search_update(const update& pages, int first, int last, int search) {
  for (int i = first; i <= last; i++) {
    if (pages[i] == search)
      return i;
  }
  return -1;
}

/*
  97,13,75,29,47
  29|13
 */
void swap_pages(update& pages, int first, int value) {
  int second = search_update(pages, 0, pages.size(), value);
  pages[second] = pages[first];
  pages[first] = value;
}

/*
  Approach 1:
  - Switch invalid pages
 */
void make_valid(update& pages, const ruleset& rules) {
  std::cout << std::endl << "START:"; dump_update(pages);
  for (int pi = 0; pi < pages.size(); pi++) {
    auto p = pages[pi];
    for (auto r : rules) {
      if (is_rule_appropriate(p, r, pages)) {
	if (search_update(pages, 0, pi, r.first) == -1) {
	  // If the rule is not met then move the required page to before the current
	  // one and then start again
	  swap_pages(pages, pi, r.first);
	  pi = -1;
	  break;
	}
      }
    }
  }

  std::cout << "NOW:"; dump_update(pages); std::cout << std::endl;
}

int main(int argc, char* argv[])
{
  lines = load_input(get_input_path() + "/5");
 int sum = 0;
  ruleset rules;

  for (auto l : lines) {
    if (is_rule(l))
      add_rule_to_set(rules, l);
    if (is_update(l)) {
      std::vector<std::string> vs; split_string(l, vs, ',');
      update pages;
      for (auto j : vs)
	pages.push_back(std::atoi(j.c_str()));
      if (is_valid_update(pages, rules)) {
	std::cout << "VALID:   " << get_middle(pages) << " ";
	// sum += get_middle(pages);
      } else {
	std::cout << "INVALID: ";
	// Make valid and then get middle
	make_valid(pages, rules);
	sum += get_middle(pages);
      }
      std::cout << l << std::endl;
    }
  }

  std::cout << "Sum of middles: " << sum << std::endl;

  return 0;
}
