/*
 * day14a
 */

#include <iostream>
#include <regex>

#include "strutils.h"
#include "io.h"
#include "board.h"

auto input = load_input(get_input_path() + "/14");

typedef int int_t;

typedef struct {
  int_t x;
  int_t y;
} position;

typedef struct
{
  position pos;
  position direction;
} robot;

int_t max_y = 103; //  7; // 103
int_t max_x = 101; // 11; // 101

std::ostream& operator << (std::ostream& o, const robot& a)
{
  o << "p:{" << a.pos.x << "," << a.pos.y << "} d:{" << a.direction.x << "," << a.direction.y << "}";
  return o;
}

typedef std::vector<robot> robot_vt;

robot_vt robots;

// p=0,4 v=3,-3
void load_robot(std::string s) {
  std::regex xy("-*[0-9]+");
  auto begin = std::sregex_iterator(s.begin(), s.end(), xy);
  int_t x = std::stoll((*begin).str()); begin++;
  int_t y = std::stoll((*begin).str()); begin++;

  robot r;
  r.pos = position{x, y};
  x = std::stoll((*begin).str()); begin++;
  y = std::stoll((*begin).str()); begin++;
  r.direction = position{x, y};

  robots.push_back(r);
  std::cout << r << std::endl;
}

void move_robot(robot& r) {
  r.pos.x += r.direction.x;
  r.pos.y += r.direction.y;
  if (r.pos.x < 0) r.pos.x += max_x;
  if (r.pos.x >= max_x) r.pos.x -= max_x;
  if (r.pos.y < 0) r.pos.y += max_y;
  if (r.pos.y >= max_y) r.pos.y -= max_y;
}

board plot_robots() {
  board b(max_y, max_x, '.');
  for (auto r : robots) {
    if (b.get_char(r.pos.y, r.pos.x) == '.')
      b.set_char(r.pos.y, r.pos.x, '1');
    else
      b.inc_char(r.pos.y, r.pos.x);
  }
  return b;
}

std::vector<int_t> count_quadrants() {
  auto mid_x = max_x/2;
  auto mid_y = max_y/2;
  std::cout << mid_x << ":" << mid_y << std::endl;

  std::vector<int_t> qs(4,0);
  for (auto r : robots) {
    if (r.pos.x < mid_x && r.pos.y < mid_y)
      qs[0]++;
    if (r.pos.x < mid_x && r.pos.y > mid_y)
      qs[1]++;
    if (r.pos.x > mid_x && r.pos.y < mid_y)
      qs[2]++;
    if (r.pos.x > mid_x && r.pos.y > mid_y)
      qs[3]++;
  }

  for (auto i : qs) {
    std::cout << i << std::endl;
  }
  return qs;
}

int main(int argc, char* argv[])
{
  for (auto r : input)
    load_robot(r);

  auto b = plot_robots();
  std::cout << "INITIAL " << robots.size() << std::endl;
  b.dump();
  std::cout << std::endl;

  const int ITERS = 100;
  for (int i = 0; i < ITERS; i++) {
    // std::cout << "Iteration " << i << std::endl;
    for (robot_vt::iterator j = robots.begin(); j != robots.end(); j++) {
      move_robot(*j);
    }
  }

  b = plot_robots();
  b.dump();

  for (auto r : robots)
    std::cout << r << std::endl;
  auto qs = count_quadrants();

  std::cout << "safety:" << qs[0] * qs[1] * qs[2] * qs[3] << std::endl;

  return 0;
}

/*
1.12...
*/
