/*
 * day4a
 */

#include <fstream>
#include <vector>
#include <string>
#include <filesystem>
#include <iostream>
#include <cstdlib>
#include <algorithm>

#include "strutils.h"
#include "io.h"

std::string input = "4";


const std::string word = "XMAS";

int rows, cols;

bool valid_coords(int x, int y) {
  return x >= 0 && x < cols && y >= 0 && y < rows;
}

char get_pos(int x, int y, std::vector<std::string> &lines) {
  if (valid_coords(x, y)) {
    std::string row = lines[y];
    // std::cout << x << "," << y << ":" << row[x] << std::endl;
    return row[x];
  }
  return 0;
}

bool trace_word(int x, int y, int x1, int y1, std::string word, std::vector<std::string>& lines) {
  if (word == "") // We've reached the end of the word
    return true;

  x += x1; y += y1;
  if (!valid_coords(x,y))	// Moved off the board
    return false;
  auto c = get_pos(x, y, lines);
  if (c == word[0]) {
    // Carry on looking for next char
    return trace_word(x, y, x1, y1, word.substr(1), lines);
  } else
    return false;
}

int main(int argc, char* argv[])
{
  auto lines = load_input(get_input_path() + "/" + input);
  cols = lines[0].length();
  rows = lines.size();

  int count = 0;
  int x, y = 0;
  while (y < rows) {
    x = 0;
    while (x < cols) {
      // std::cout << x << "," << y << std::endl;
      if (get_pos(x, y, lines) == word[0]) {
	if (trace_word(x, y, 0, 1, word.substr(1), lines)) count++;
	if (trace_word(x, y, 1, 1, word.substr(1), lines)) count++;
	if (trace_word(x, y, 1, 0, word.substr(1), lines)) count++;
	if (trace_word(x, y, 1, -1, word.substr(1), lines)) count++;
	if (trace_word(x, y, 0, -1, word.substr(1), lines)) count++;
	if (trace_word(x, y, -1, -1, word.substr(1), lines)) count++;
	if (trace_word(x, y, -1, 0, word.substr(1), lines)) count++;
	if (trace_word(x, y, -1, 1, word.substr(1), lines)) count++;
      }
      x++;
    }
    y++;
  }

  std::cout << "Count: " << count << std::endl;
  return 0;
}
