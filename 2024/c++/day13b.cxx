/*
 * day13a
 */

#include <iostream>
#include <regex>

#include "strutils.h"
#include "io.h"

typedef unsigned long inttype;
typedef std::pair<inttype, inttype> pair_t;
const int max_iter = 100000;

pair_t calculate_counts(inttype px, inttype ax, inttype ay, inttype py, inttype bx, inttype by) {
  // inttype ca = (py - by*px/bx)/(ay-by*ax/bx);
  double axd = ax, ayd = ay, bxd = bx, byd = by;
  double cad = (bxd * py - byd * px)/(bxd * ayd - byd * ax);
  double cbd = (px - cad*axd)/bxd;
  return pair_t{cad, cbd};

  inttype ca = (bx * py - by * px)/(bx * ay - by * ax);
  inttype cb = (px - ca*ax)/bx;
  std::cout << "A:" << ca << " B:" << cb << std::endl;
  return pair_t{ca, cb};
}

bool calculate_solution(inttype ca, inttype cb, inttype px, inttype ax, inttype ay, inttype py, inttype bx, inttype by) {
  return (px == ca * ax + cb * bx && py == ca * ay + cb * by);
}

pair_t iterate_solutions(inttype px, inttype ax, inttype ay, inttype py, inttype bx, inttype by) {
  for (int ca = 1; ca <= max_iter; ca++)
    for (int cb = 1; cb <= max_iter; cb++) {
      if (calculate_solution(ca, cb, px, ax, ay, py, bx, by))
	return std::pair<int, int>(ca, cb);
    }
  return pair_t(0,0);
}

auto input = load_input(get_input_path() + "/13");

/*
Button A: X+94, Y+34
Button B: X+22, Y+67
Prize: X=8400, Y=5400
*/

pair_t extract_xy(std::string s) {
  std::regex xy("[0-9]+");
  auto words_begin = std::sregex_iterator(s.begin(), s.end(), xy);
  auto words_end = std::sregex_iterator();

  std::string sx = (*words_begin).str();
  words_begin++;
  std::string sy = (*words_begin).str();
  return pair_t(std::stoll(sx), std::stoll(sy));
}

int main(int argc, char* argv[])
{
  inttype cost = 0;
  for (int r = 0; r < input.size(); r++) {
    // Assume 3 lines per game + 1 blank
    std::string a = input[r++];
    auto xy = extract_xy(a);
    inttype ax = xy.first;
    inttype ay = xy.second;
    std::string b = input[r++];
    xy = extract_xy(b);
    inttype bx = xy.first;
    inttype by = xy.second;
    std::string prize = input[r];
    xy = extract_xy(prize);
    inttype px = xy.first + 10000000000000;
    inttype py = xy.second + 10000000000000;

    auto c = calculate_counts(px, ax, ay, py, bx, by);
    // auto c = iterate_solutions(px, ax, ay, py, bx, by);

    std::cout << px << " = " << c.first << " * " << ax << " + " << c.second << " * " << bx << " = " << c.first * ax + c.second * bx << std::endl;
    if (px == c.first * ax + c.second * bx &&
	py == c.first * ay + c.second * by) {
      std::cout << "solved: A=" << c.first << " B=" << c.second << std::endl;
      cost += 3 * c.first + c.second;
    }
    else
      std::cout << "not solved" << std::endl;
  }

  // auto c = calculate_counts(8400, 94, 34, 5400, 22, 67);
  // cost = 3 * c.first + c.second;
  std::cout << "Cost:" << cost << std::endl;
  return 0;
}

/*
 */
