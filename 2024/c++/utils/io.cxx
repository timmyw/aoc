
#include "io.h"

#include <string>
#include <iostream>
#include <fstream>
#include <filesystem>

#include "strutils.h"

std::string load_input_whole(std::string filename) {
  std::ifstream f(filename,  std::ios::in);
  if (!f.is_open()) {
    std::cout << "Error opening:" << filename << std::endl;
    return "";
  }

  const auto sz = std::filesystem::file_size(filename);
  std::string result(sz, '\0');
  f.read(result.data(), sz);

  return result;
}

std::vector<std::string> load_input(std::string filename) {
  std::vector<std::string> lines;
  std::string result = load_input_whole(filename);

  auto c = split_string(result, lines, '\n');

  return lines;
}
