
#include <algorithm>

#include "board.h"
#include "io.h"

board::board() :
  cols(0),
  rows(0)
{

}

board::board(int r, int c, char init) {
  rows = r;
  cols = c;
  for (int i = 0; i < r; i++) {
    std::string s(c, init);
    lines.push_back(s);
  }
}

void
board::dump(int char_offset /* = 0 */) {
  for (auto r : lines) {
    if (!char_offset)
      std::cout << r;
    else {
      for (int i = 0; i < r.size(); i++) {
	char c = '.';
	if (r[i] > 0)
	  c = r[i]+char_offset;
	std::cout << c;
      }
    }
    std::cout << std::endl;
  }
}

void
translate(std::string &s) {
  std::transform(s.begin(), s.end(), s.begin(), [](char c) { return c == 0?'.':c; });
}

void
board::dump(const board& other) {
  for (int i = 0; i < lines.size(); i++) {
    auto l = lines[i]; translate(l);
    auto o = other.lines[i]; translate(o);
    std::cout << l << "  ->  " << o << std::endl;
  }
}

int
board::count(char c) const {
  int count = 0;
  for (auto l : lines) {
    for (int i = 0; i < l.size(); i++)
      if (l[i] == c)
	count++;
  }
  return count;
}

int
board::count_not(char c) const {
  int count = 0;
  for (auto l : lines) {
    for (int i = 0; i < l.size(); i++)
      if (l[i] != c)
	count++;
  }
  return count;
}

bool
board::valid_coords(int r, int c) const {
  return c >= 0 && c < cols && r >= 0 && r < rows;
}

bool
board::valid_coords(coord& c) const {
  return valid_coords(c.r, c.c);
}

char
board::get_char(int r, int c) const {
  if (!valid_coords(r,c))
    return '~';
  auto row = lines[r];
  return row[c];
}

char
board::get_char(coord& c) const {
  return get_char(c.r, c.c);
}

void
board::set_char(int r, int c, char t) {
  if (!valid_coords(r, c))
    return;
  auto row = lines[r];
  row[c] = t;
  lines[r] = row;
}

void
board::inc_char(int r, int c) {
  if (!valid_coords(r, c))
    return;
  char ch = get_char(r, c);
  set_char(r, c, ++ch);
}

void
board::set_char(coord& c, char t) {
  set_char(c.r, c.c, t);
}

coord
board::find(int start_row, int start_col, char ch) const {
  for (int r = start_row; r < rows; r++)
    for (int c = start_col; c < cols; c++)
      if (get_char(r, c) == ch)
	return coord{r, c};
  return coord{-1, -1};
}

void
board::add_row(std::string s)
{
  lines.push_back(s);
  rows = lines.size();
  cols = s.length();
}

board
load_board(std::string filename) {
  board b;

  b.lines = load_input(filename);
  b.cols = b.lines[0].length();
  b.rows = b.lines.size();

  return b;
}
