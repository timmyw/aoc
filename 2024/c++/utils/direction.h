#if !defined(_DIRECTION_H_)
#define _DIRECTION_H_

extern coord DIRECTIONS[];

int get_dir_index(coord& dir);
coord rotate_clockwise(coord& dir);
coord rotate_anti_clockwise(coord& dir);

#endif // _DIRECTION_H_
