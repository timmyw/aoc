
#include "coord.h"

std::ostream& operator << (std::ostream& o, const coord& a)
{
  o << "{" << a.r << ", " << a.c << "}";
  return o;
}

coord find_char(char c, const std::vector<std::string>& lines) {
  int r = 0;
  for (auto l : lines) {
    int c;
    if ((c = l.find(c)) != std::string::npos) {
      return coord{r, c};
    }
    r++;
  }
  return coord{-1, -1};
}

const coord dir_up =    {-1, 0};
const coord dir_right = {0,  1};
const coord dir_down  = {1,  0};
const coord dir_left  = {0, -1};
