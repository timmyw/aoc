#if !defined(_IO_H_)
#define _IO_H_

#include <vector>
#include <string>

// Loading input
std::vector<std::string> load_input(std::string filename);

std::string load_input_whole(std::string filename);

#endif // _IO_H_
