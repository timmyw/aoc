
#include "coord.h"
#include "direction.h"

coord DIRECTIONS[] = {
  {  0, 1},
  {  1, 0},
  {  0, -1},
  { -1, 0}
};

int
get_dir_index(coord& dir) {
  for (int i = 0; i < sizeof(DIRECTIONS); i++) {
    if (DIRECTIONS[i] == dir)
      return i;
  }
  return -1;
}

coord
rotate_clockwise(coord& dir) {
  if (int i = get_dir_index(dir); i != -1) {
    i = (i + 1) % 4;
    return DIRECTIONS[i];
  }
  return dir;
}

coord
rotate_anti_clockwise(coord& dir) {
  if (int i = get_dir_index(dir); i != -1) {
    i = (i + 3) % 4;
    return DIRECTIONS[i];
  }
  return dir;
}
