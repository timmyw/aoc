#if !defined(_COORD_H_)
#define _COORD_H_

#include <iostream>
#include <vector>

struct coord {
  int r;
  int c;

  coord(int r_, int c_) : r(r_), c(c_) {}

  coord operator+(coord const& rhs) { return coord{r+rhs.r, c+rhs.c}; }

  coord operator+=(coord const& rhs) { return coord{r+rhs.r, c+rhs.c}; }

  bool operator==(coord const& rhs) const { return r==rhs.r && c==rhs.c; }
}; // coord;

extern const coord dir_up;
extern const coord dir_right;
extern const coord dir_down;
extern const coord dir_left;

std::ostream& operator << (std::ostream& o, const coord& a);
coord find_char(char c, const std::vector<std::string>& lines);

#endif // _COORD_H_
