#if !defined(_STRUTILS_H_)
#define _STRUTILS_H_

#include <string>
#include <vector>

int split_string(std::string inp,
		 std::vector<std::string> &out,
		 char delim,
		 bool ignore_blank = true);

std::string get_input_path();

#endif //
