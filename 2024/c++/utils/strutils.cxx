
#include <string>
#include <iostream>
#include <filesystem>
#include <sstream>
#include <vector>
#include <algorithm>

#include "string.h"

inline void rtrim(std::string &s) {
    s.erase(std::find_if(s.rbegin(), s.rend(), [](unsigned char ch) {
        return !std::isspace(ch) && ch != '\n' && ch != '\r';
    }).base(), s.end());
}

int split_string(std::string inp, std::vector<std::string> &out, char delim, bool ignore_blank /* = true */) {
  std::istringstream ss(inp);
  std::string s;
  int i = 0;
  while (std::getline(ss, s, delim)) {
    if (!ignore_blank || s.length()) {
      i++;
      rtrim(s);
      out.push_back(s);
    }
  }
  // std::cout << "split_string:" << i << ":" << out.size() << std::endl;
  return out.size();
}

std::string get_input_path() {
  /* Seriously ugly */
  auto p = std::filesystem::current_path();
  std::string ps = p;
  while (ps.length() >= 4 && ps.substr(ps.length()-4, 4) != "/c++") {
    p = p.parent_path();
    ps = p;
  }
  ps = p.parent_path();
  return ps + "/input";
}
