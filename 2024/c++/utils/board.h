#if !defined(_BOARD_H_)
#define _BOARD_H_

#include "coord.h"

class board {
 public:
  board();
  board(int r, int c, char init);

  void add_row(std::string s);

  int count(char c) const;
  int count_not(char c) const;

  bool valid_coords(int x, int y) const;
  bool valid_coords(coord& c) const;

  char get_char(int r, int c) const;
  char get_char(coord& c) const;
  void set_char(coord& c, char t);
  void set_char(int r, int c, char t);
  void inc_char(int r, int c);

  void dump(int char_offset = 0);
  void dump(const board& other);

  coord find(int start_row, int start_col, char ch) const;

 public:
  std::vector<std::string> lines;
  int cols;
  int rows;
};

board load_board(std::string filename);

#endif
