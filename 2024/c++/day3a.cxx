/*
 * day3a
 */

#include <regex>
#include <string>
#include <iostream>

#include "strutils.h"
#include "io.h"

std::string input = "3";

int extract_multiply(std::string e) {
  std::regex mult("[0-9]+");
  auto words_begin = std::sregex_iterator(e.begin(),
					  e.end(),
					  mult);
  auto words_end = std::sregex_iterator();
  int x = std::atoi((*words_begin).str().c_str());
  words_begin++;
  int y = std::atoi((*words_begin).str().c_str());
  return x * y;
}

int main(int argc, char* argv[])
{
  std::string inp = load_input_whole(get_input_path() + "/" + input);

  std::cout << "INPUT:" << inp << std::endl;

  //  std::regex expr("mul([0-9]+,[0-9]+)");
  std::regex expr("mul\\([0-9]+,[0-9]+\\)");
  auto words_begin = std::sregex_iterator(inp.begin(),
					  inp.end(),
					  expr);
  auto words_end = std::sregex_iterator();

  int sum = 0;
  for (std::sregex_iterator i = words_begin; i != words_end; ++i) {
    std::smatch match = *i;
    std::string match_str = match.str();
    std::cout << match_str << '\n';
    sum += extract_multiply(match_str);
  }

  std::cout << "Sum:" << sum << std::endl;

  return 0;
}
