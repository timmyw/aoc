/*
 * day7a
 */

#include <vector>
#include <string>
#include <iostream>
#include <utility>
#include <stdint.h>

#include "strutils.h"
#include "io.h"

std::vector<std::string> lines;
int rows, cols;

typedef int64_t inttype;
typedef std::vector<inttype> v_inttype;

void dump_nums(v_inttype vs) {
  for (auto i : vs) { std::cout << i << " "; }
  // std::cout << std::endl;
}

inttype check_perms(inttype t, v_inttype vs) {
  // Treat as a binary number - 0=add, 1=mult
  std::cout << t << " ?= ";
  dump_nums(vs);
  inttype bin_tot = 1 << (unsigned)(vs.size() - 1);
  // std::cout << "Binary tot:" << bin_tot << std::endl;
  for (int i = 0; i < bin_tot; i++) {
    // Set the operators
    char ops[vs.size()+2];
    for (int j = 0; j < vs.size() - 1; j++) {
      // If the positional bit is set in i then this is a mult otherwise a plus
      inttype q = (1 << j);
      // std::cout << i << " 1 << " << j << "=" << q << " " << (i & q) << std::endl;
      ops[j] = (i & q) == 0 ? '+':'*'; // Get binary digit at j
    }
    ops[vs.size()] = 0;
    // std::cout << i << " ops:" << ops << std::endl;

    // int m = 1;
    // std::cout << vs[0] << ops[0];
    // for (int k = 1; k < vs.size(); k++) {
    //   std::cout << vs[k] << ops[m++];
    // }
    // std::cout << '=';

    inttype res = vs[0];
    int r = 0;
    for (int j = 1; j < vs.size(); j++) {
      if (ops[r++] == '+')
	res = res + vs[j];
      else
	res = res * vs[j];
    }
    // std::cout << res << (res==t ? "==" : "!=" ) << t << std::endl;
    if (t == res) {
      std::cout << " solved" << std::endl;
      return t;
    }
  }
  std::cout << " unsolved" << std::endl;
  return 0;
}

// 3267: 81 40 27
inttype possible(std::string l) {
  std::vector<std::string> ll; split_string(l, ll, ':');
  inttype total = (inttype)std::atoll(ll[0].c_str());
  std::vector<std::string> vs; split_string(ll[1], vs, ' ');
  v_inttype vv;
  for (auto i : vs) {
    inttype u = std::atoll(i.c_str());
    vv.push_back(u);
  }

  return check_perms(total, vv);
}


int main(int argc, char* argv[])
{
  lines = load_input(get_input_path() + "/7");
  rows = lines.size(); cols = lines[0].length();

  // std::string s = "64288413730";
  // inttype i = std::atoll(s.c_str());
  // std::cout << i << std::endl;
  // return 0;

  // for (int i = 0; i < 8; i++)
  //   std::cout << i << ":" << (1 << i) << std::endl;
  // return 0;

  // 230456169: 3 630 12 7 2 1 6 26 3 69

  inttype sum = 0;
  // sum = possible("230456169: 3 630 12 7 2 1 6 26 3 69");
  for (auto l : lines) {
    sum += possible(l);
  }
  std::cout << "Sum: " << sum << std::endl;

  return 0;
}

/*
45238932987
93939236205
3744922989
3744922989
*/
