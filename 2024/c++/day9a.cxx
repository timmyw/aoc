/*
 * day9a
 */

#include <regex>
#include <string>
#include <iostream>

#include "strutils.h"
#include "io.h"

std::string inp = load_input_whole(get_input_path() + "/9");

typedef int64_t inttype;
typedef std::vector<inttype> inttype_v;

void dump_v(const inttype_v& v) {
  for (auto i : v) {
    if ((int)i != -1)
      std::cout << i;
    else
      std::cout << '.';
  }
  std::cout << std::endl;
}

void build_diskmap(std::string l, inttype_v& dm) {
  bool file = false;
  int cur = 0;
  unsigned id = 0;
  while (cur < l.size()) {
    char c = l[cur];
    file = !file; cur++;
    if (c < 32)
      continue;
    int i = ((int)c) - 48;
    auto conc = inttype_v(i, file?id:-1);
    if (file)
      id++;
    // std::cout << i << ":" << file << " "; dump_v(conc); std::cout << std::endl
							  ;
    dm.insert(dm.end(), conc.begin(), conc.end());
    // dump_v(dm); std::cout << std::endl;
    // if (id>=2) return;
  }
}

inttype find_last_file(const inttype_v& m) {
  inttype r = m.size()-1;
  std::cout << m[r] << std::endl;
  return r;
}

void defrag_disk(inttype_v& dm) {
  inttype left = 0;
  inttype right = find_last_file(dm);
  std::cout << "right:" << right << std::endl;
  while (left < dm.size() && right > left) {
    if ((int)dm[left] == -1) {
      // We have a space - fill from the far right
      dm[left] = dm[right];
      dm[right] = -1;
      while ((int)dm[--right] == -1) ;
      // right--;
    }
    left++;
  }
}

inttype calc_checksum(const inttype_v& dm) {
  inttype cs = 0;
  int i = 0;
  while (i < dm.size() && (int)dm[i] != -1) {
    // std::cout << i << ":" << dm[i] << "=" << i * dm[i] << std::endl;
    cs += i * dm[i];
    i++;
  }
  return cs;
}

int main(int argc, char* argv[])
{
  inttype_v dm;
  build_diskmap(inp, dm);
  std::cout << inp << std::endl;
  dump_v(dm);
  defrag_disk(dm);
  dump_v(dm);
  std::cout << "checksum:" << calc_checksum(dm) << std::endl;
  return 0;
}

/*
0099811188827773336446555566
0099811188827773336446555566
 */

/*
  dump_v(dm); std::cout << std::endl;
  std::vector<inttype> v1{1,2,3};
  dump_v(v1); std::cout << std::endl;
  dm.insert(dm.end(), v1.begin(), v1.end());
  dump_v(dm); std::cout << std::endl;
  return 0;
*/
