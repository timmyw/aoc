/*
 * day2b
 */

#include <fstream>
#include <vector>
#include <string>
#include <filesystem>
#include <iostream>
#include <cstdlib>
#include <algorithm>

#include "strutils.h"
#include "io.h"

std::string input = "2";

int split_strings(std::vector<std::string> inp, std::vector<int> &left, std::vector<int> &right) {
  for (auto i : inp) {
    std::vector<std::string> nums;
    split_string(i, nums, ' ');
    left.push_back(atoi(nums[0].c_str()));
    right.push_back(atoi(nums[1].c_str()));
  }
  return left.size();
}

std::vector<int> parse_report(const std::string &l) {
  std::vector<int> x;
  std::vector<std::string> xs;

  split_string(l, xs, ' ');
  for (auto i : xs) {
    int j = std::atoi(i.c_str());
    x.push_back(j);
  }
  return x;
}

int sign(int i) {
  if (i<0) return -1;
  if (i>0) return 1;
  return 0;
}

void dump_report(std::vector<int> &report) {
  for (auto i : report) {
    std::cout << i << " ";
  }
}

bool is_report_safe(std::vector<int> report) {
  int i1 = 0, i2 = 1;
  int dir = report[i1] - report[i2];
  dump_report(report); std::cout << " [" << report[i1] << "," << report[i2] << "] :" << dir << " ";
  int unsafe_count = 0;
  while (i1 < report.size() - 2) {
    int adir = abs(dir);
    if (adir == 0 || adir > 3) {
      std::cout << "NOT123" << std::endl;
      return false;
    }
    i1 = i2; i2++;
    int new_dir = report[i1] - report[i2];
    std::cout << "[" << report[i1] << "," << report[i2] << "] :" << new_dir << " ";
    if (sign(new_dir) != sign(dir)) {  // Direction is changing
      std::cout << "CHANGED" << std::endl;
      return false;
    }
    dir = new_dir;
  }
  int adir = abs(dir);
  if (adir == 0 || adir > 3) {
    std::cout << "NOT123" << std::endl;
    return false;
  }
  std::cout << "TRUE" << std::endl;
  return true;
}

std::vector<int> drop_element(std::vector<int>& report, int idx) {
  auto cp = report;
  cp.erase(std::next(cp.begin(), idx));
  return cp;
}

bool check_all_report(std::vector<int> report) {
  /* run a plan is_report_safe on each iteration of the report created by removing
     one element
   */
  if (is_report_safe(report))
    return true;
  for (int i = 0; i < report.size(); i++) {
    auto rep = drop_element(report, i);
    if (is_report_safe(rep))
      return true;
  }
  return false;
}

int main(int argc, char* argv[])
{
  auto input_path = get_input_path() + "/" + input;
  auto lines = load_input(input_path);
  std::vector<std::vector<int>> reports;
  for (auto i : lines) {
    auto report = parse_report(i);
    reports.push_back(report);
  }

  int safe_count = 0;
  int i = 0;
  for (auto report : reports) {
    if (check_all_report(report))
      safe_count++;
    i++;
  }

  std::cout << "Safe count:" << safe_count << std::endl;

  return 0;
}
