/*
 * day7b
 */

#include <vector>
#include <string>
#include <iostream>
#include <utility>
#include <stdint.h>
#include <math.h>

#include "strutils.h"
#include "io.h"

std::vector<std::string> lines;
int rows, cols;

typedef int64_t inttype;
typedef std::vector<inttype> v_inttype;

void dump_nums(v_inttype vs) {
  for (auto i : vs) { std::cout << i << " "; }
  // std::cout << std::endl;
}

/*
  3 operators - using ternary will be a pain in the butt.
  Start the operators with 0
  then increment first.  If > 2 then
    reset to 0, inc next (if there is one)
      if > 2 then ...
 */

char inc_op(char c) {
  switch (c) {
  case '+': return '*';
  case '*': return '|';
  }
  return '_';
}

void inc_operators(char* ops, int start, int size) {
  if (start >= size)
    return;

  ops[start] = inc_op(ops[start]);
  if (ops[start] == '_') {
    ops[start] = '+';
    inc_operators(ops, start+1, size);
  }
}

void init_operators(char* ops, int size) {
  for (int i = 0; i < size; i++)
    ops[i] = '+';
}

inttype check_perms(inttype t, v_inttype vs) {
  // Treat as a binary number - 0=add, 1=mult
  std::cout << t << " ?= ";
  dump_nums(vs);
  int op_size = vs.size()-1;
  int iter_total = pow(3 ,op_size);
  char ops[op_size+2]; init_operators(ops, op_size);
  for (int i = 0; i < iter_total; i++) {
    // Set the operators
    // std::cout << i << " ops:" << ops << std::endl;

    // int m = 1;
    // std::cout << vs[0] << ops[0];
    // for (int k = 1; k < vs.size(); k++) {
    //   std::cout << vs[k] << ops[m++];
    // }
    // std::cout << '=';

    inttype res = vs[0];
    int r = 0;
    for (int j = 1; j < vs.size(); j++) {
      if (ops[r] == '+')
	res = res + vs[j];
      else if (ops[r] == '*') {
	res = res * vs[j];
      } else if (ops[r] == '|') {
	char buf[129];
	snprintf(buf, 128, "%lld%lld", res, vs[j]);
	res = atoll(buf);
      }
      r++;
    }
    // std::cout << res << (res==t ? "==" : "!=" ) << t << std::endl;
    if (t == res) {
      std::cout << " solved" << std::endl;
      return t;
    }

    inc_operators(ops, 0, op_size);
  }
  std::cout << " unsolved" << std::endl;
  return 0;
}

// 3267: 81 40 27
inttype possible(std::string l) {
  std::vector<std::string> ll; split_string(l, ll, ':');
  inttype total = (inttype)std::atoll(ll[0].c_str());
  std::vector<std::string> vs; split_string(ll[1], vs, ' ');
  v_inttype vv;
  for (auto i : vs) {
    inttype u = std::atoll(i.c_str());
    vv.push_back(u);
  }

  return check_perms(total, vv);
}


int main(int argc, char* argv[])
{
  lines = load_input(get_input_path() + "/7");
  rows = lines.size(); cols = lines[0].length();
  // std::string s = "64288413730";
  // inttype i = std::atoll(s.c_str());
  // std::cout << i << std::endl;
  // return 0;

  // for (int i = 0; i < 8; i++)
  //   std::cout << i << ":" << (1 << i) << std::endl;
  // return 0;

  // 230456169: 3 630 12 7 2 1 6 26 3 69

  inttype sum = 0;
  // sum = possible("230456169: 3 630 12 7 2 1 6 26 3 69");
  for (auto l : lines) {
    sum += possible(l);
  }
  std::cout << "Sum: " << sum << std::endl;

  return 0;
}

/*
4122618559853
45238932987
93939236205
3744922989
3744922989
*/
