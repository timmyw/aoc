/*
 * day9b
 */

#include <regex>
#include <string>
#include <iostream>
#include <boost/format.hpp>

#include "strutils.h"
#include "io.h"

std::string inp = load_input_whole(get_input_path() + "/9");

typedef int64_t inttype;
typedef std::vector<inttype> inttype_v;

void dump_v(const inttype_v& v) {
  for (auto i : v) {
    if ((int)i != -1)
      std::cout << i;
    else
      std::cout << '.';
  }
  std::cout << std::endl;
}

void build_diskmap(std::string l, inttype_v& dm) {
  bool file = false;
  int cur = 0;
  unsigned id = 0;
  while (cur < l.size()) {
    char c = l[cur];
    file = !file; cur++;
    if (c < 32)
      continue;
    int i = ((int)c) - 48;
    auto conc = inttype_v(i, file?id:-1);
    if (file)
      id++;
    // std::cout << i << ":" << file << " "; dump_v(conc); std::cout << std::endl
							  ;
    dm.insert(dm.end(), conc.begin(), conc.end());
    // dump_v(dm); std::cout << std::endl;
    // if (id>=2) return;
  }
}

inttype find_last_file(const inttype_v& m) {
  inttype r = m.size()-1;
  std::cout << m[r] << std::endl;
  return r;
}

inttype find_next_file(inttype left, inttype& right, inttype_v& dm, inttype& file_id, int& filesize) {
  filesize = 0;
  while ((int)dm[right] == -1 && right > left)
    right--;			// Now at the end of last file
  if (right <= left)
    return 0;
  file_id = dm[right];

  // std::cout << "file_id:" << file_id << std::endl;
  while (dm[right] == file_id) {
    std::cout << "find dm[right]:" << dm[right] << " " << right << " " << left << std::endl;
    right--;
    filesize++;
  }
  right++; 			// Jump back over the first non-id block
  std::cout << "end of last:" << right << " " << dm[right] << std::endl;
  return file_id;
}

inttype find_gap(inttype left, inttype right, int filesize, inttype_v& dm) {
  inttype gap = left;
  int count;
  // Count forward from current to find at least ~filesize~ free blocks
  while (left < right) {
    while ((int)dm[left] != -1)
      left++;			// Found the beginning of free blocks
    count = filesize;
    while (count > 0 && (int)dm[left] == -1) {
      count--;
      left++;
    }
    if (!count)			// We have found enough space
      return left - filesize;
  }
  return 0;			// Not entirely valid as a "not found" status
}

void defrag_disk(inttype_v& dm) {
  inttype left = 0;
  inttype right = find_last_file(dm);
  std::cout << "right:" << right << std::endl;
  while (left < dm.size() && right > left) {
    inttype id;
    int filesize;
    find_next_file(left, right, dm, id, filesize);
    std::cout << "next file:" << right << " [" << id << "(" << filesize << ")]" << std::endl;
    if (id != 0) {
      auto gap = find_gap(left, right, filesize, dm);
      std::cout << boost::format("gap %1% (%2%)") % gap % filesize << std::endl;
      if (gap && gap < right) { // move the file from right to right+filesize to left.  If no gap
		 // then keep moving 'right' left
	auto r = right;
	while (filesize--) {
	  std::cout << "moving " << dm[r] << " to " << (int)dm[gap] << std::endl;
	  dm[gap++] = dm[r];
	  dm[r++] = -1;
	}
      } else
	right--;
    }
  }
}

inttype calc_checksum(const inttype_v& dm) {
  inttype cs = 0;
  int i = 0;
  while (i < dm.size() ) {
    // std::cout << i << ":" << dm[i] << "=" << i * dm[i] << std::endl;
    if ((int)dm[i] != -1)
      cs += i * dm[i];
    i++;
  }
  return cs;
}

int main(int argc, char* argv[])
{
  inttype_v dm;
  build_diskmap(inp, dm);
  auto dm_2 = dm;
  std::cout << inp << std::endl;
  dump_v(dm);
  defrag_disk(dm);
  dump_v(dm_2);
  dump_v(dm);
  std::cout << "checksum:" << calc_checksum(dm) << std::endl;
  return 0;
}

/*
  00992111777.44.333....5555.6666.....8888..
  00992111777.44.333....5555.6666.....8888..
*/
