/*
 * day6a
 */

#include <vector>
#include <string>
#include <iostream>
#include <utility>

#include "coord.h"
#include "strutils.h"
#include "io.h"

std::vector<std::string> lines;

int cols, rows;

coord find_char(char c) {
  int y = 0;
  for (auto l : lines) {
    int x;
    if ((x = l.find(c)) != std::string::npos) {
      return coord{x, y};
    }
    y++;
  }
  return coord{-1, -1};
}

coord find_start() {
  auto c = find_char('^');
  if (c.r == -1)
    c = find_char('>');
  if (c.r == -1)
    c = find_char('v');
  if (c.r == -1)
    c = find_char('<');
  return c;
}

bool valid_coords(int r, int c) {
  return c >= 0 && c < cols && r >= 0 && r < rows;
}

bool valid_coords(coord& c) {
  return valid_coords(c.r, c.c);
}

char get_char(int x, int y) {
  if (!valid_coords(x,y))
    return '~';
  auto r = lines[y];
  return r[x];
}

char get_char(coord& c) {
  return get_char(c.r, c.c);
}

void set_char(coord& c, char t) {
  if (!valid_coords(c))
    return;
  auto r = lines[c.r];
  r[c.c] = t;
  lines[c.r] = r;
}

coord get_direction(char c) {
  switch (c) {
  case '^': return coord{0, -1};
  case '>': return coord{1, 0};
  case '<': return coord{-1, 0};
  case 'v': return coord{0, 1};
  }
  return coord{0,0};
}

char get_next_direction(char c) {
  switch (c) {
  case '^': return '>';
  case '>': return 'v';
  case 'v': return '<';
  case '<': return '^';
  }
  return '_';
}

void dump_board() {
  for (auto r : lines) {
    std::cout << r << std::endl;
  }
}

int count_x() {
  int cnt = 0;
  for (auto r : lines) {
    for (int i = 0; i < r.length(); i++)
      if (r[i] == 'X')
	cnt++;
  }
  return cnt;
}

int main(int argc, char* argv[])
{
  lines = load_input(get_input_path() + "/6");
  cols = lines[0].length();
  rows = lines.size();

  int count = 0;
  int iter = 0;

  coord cur = find_start(), old;
  while (/*iter++ < 100 && */ valid_coords(cur)) {
    // Find current position
    cur = old = find_start();
    // Store direction
    char c = get_char(cur);
    std::cout << iter << ":" << "cur:" << cur << "=" << c << std::endl;
    // Mark with X
    set_char(cur, 'X');
    // Find direction and update position
    coord dir = get_direction(c);
    cur.r += dir.r; cur.c += dir.c;
    std::cout << iter << ":" << "new:" << cur << std::endl;

    if (valid_coords(cur)) {
      char n = get_char(cur);
      if (n == '#') { 		// Obstacle, turn to the right
	cur = old;
	c = get_next_direction(c);
	std::cout << "BUMP BACK:" << cur << ":" << c << std::endl;
      }
      // Place new start/direction
      set_char(cur, c);
    }
  }

  dump_board();

  // Count up the Xs
  std::cout << "Count: " << count_x() << std::endl;
  return 0;
}
