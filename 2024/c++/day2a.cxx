/*
 * day2a
 */

#include <fstream>
#include <vector>
#include <string>
#include <filesystem>
#include <iostream>
#include <cstdlib>
#include <algorithm>

#include "strutils.h"
#include "io.h"

std::string input = "2";

int split_strings(std::vector<std::string> inp, std::vector<int> &left, std::vector<int> &right) {
  for (auto i : inp) {
    std::vector<std::string> nums;
    split_string(i, nums, ' ');
    left.push_back(atoi(nums[0].c_str()));
    right.push_back(atoi(nums[1].c_str()));
  }
  return left.size();
}

std::vector<int> parse_report(const std::string &l) {
  std::vector<int> x;
  std::vector<std::string> xs;

  split_string(l, xs, ' ');
  for (auto i : xs) {
    int j = std::atoi(i.c_str());
    x.push_back(j);
  }
  return x;
}

int sign(int i) {
  if (i<0) return -1;
  if (i>0) return 1;
  return 0;
}

bool is_report_safe(std::string &line, std::vector<int> report, int idx) {
  int i1 = 0, i2 = 1;
  int dir = report[i1] - report[i2];
  std::cout << line << " [" << report[i1] << "," << report[i2] << "] :" << dir << " ";
  while (i1 < report.size() - 2) {
    int adir = abs(dir);
    if (adir == 0 || adir > 3) {
      std::cout << "NOT123" << std::endl;
      return false;
    }
    i1 = i2; i2++;
    int new_dir = report[i1] - report[i2];
    std::cout << "[" << report[i1] << "," << report[i2] << "] :" << new_dir << " ";
    if (sign(new_dir) != sign(dir)) {  // Direction is changing
      std::cout << "CHANGED" << std::endl;
      return false;
    }
    dir = new_dir;
  }
  int adir = abs(dir);
  if (adir == 0 || adir > 3) {
    std::cout << "NOT123" << std::endl;
    return false;
  }
  std::cout << "TRUE" << std::endl;
  return true;
}

int main(int argc, char* argv[])
{
  auto input_path = get_input_path() + "/" + input;
  auto lines = load_input(input_path);
  std::vector<std::vector<int>> reports;
  for (auto i : lines) {
    auto report = parse_report(i);
    reports.push_back(report);
  }

  int safe_count = 0;
  int i = 0;
  for (auto report : reports) {
    if (is_report_safe(lines[i], report, i)) {
      safe_count++;
    }
    i++;
  }

  std::cout << "Safe count:" << safe_count << std::endl;

  return 0;
}
