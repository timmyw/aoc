/*
 * day4b
 */

#include <fstream>
#include <vector>
#include <string>
#include <filesystem>
#include <iostream>
#include <cstdlib>
#include <algorithm>

#include "strutils.h"
#include "io.h"

std::string input = "4";

const std::string word = "XMAS";

int rows, cols;

bool valid_coords(int x, int y) {
  return x >= 0 && x < cols && y >= 0 && y < rows;
}

char get_pos(int x, int y, std::vector<std::string> &lines) {
  if (valid_coords(x, y)) {
    std::string row = lines[y];
    // std::cout << x << "," << y << ":" << row[x] << std::endl;
    return row[x];
  }
  return 0;
}

bool trace_word(int x, int y, int x1, int y1, std::string word, std::vector<std::string>& lines) {
  if (word == "") // We've reached the end of the word
    return true;

  x += x1; y += y1;
  if (!valid_coords(x,y))	// Moved off the board
    return false;
  auto c = get_pos(x, y, lines);
  if (c == word[0]) {
    // Carry on looking for next char
    return trace_word(x, y, x1, y1, word.substr(1), lines);
  } else
    return false;
}

int main(int argc, char* argv[])
{
  auto lines = load_input(get_input_path() + "/" + input);
  cols = lines[0].length();
  rows = lines.size();

  int count = 0;
  int x, y = 1; 		// We have to start at least one row in
  while (y < rows - 1) { 	// And end one row short
    x = 1;			// Same for columns
    while (x < cols - 1) {	// And ending one column in
      // std::cout << x << "," << y << std::endl;
      /*
	Find an 'A' then check the possibilites.  We need an 'X'
       */
      if (get_pos(x, y, lines) == 'A') {
	auto top_left = get_pos(x - 1, y - 1, lines),
	  top_right = get_pos(x + 1, y - 1, lines),
	  bot_left = get_pos(x - 1, y + 1, lines),
	  bot_right = get_pos(x + 1, y + 1, lines);


	if ((top_left == 'M' && bot_right == 'S' || top_left == 'S' && bot_right == 'M') &&
	    (bot_left == 'M' && top_right == 'S' || bot_left == 'S' && top_right == 'M'))
	  {
	    std::cout << std::setw(3) <<
	      x << "," << y << ": "
		      << top_left << "." << top_right << std::endl
		      << "       " << "." << get_pos(x,y,lines) << "." << std::endl
		      << "       " << bot_left << "." << bot_right << std::endl;
	    count++;
	  }
      }
      x++;
    }
    y++;
  }

  std::cout << "Count: " << count << std::endl;
  return 0;
}
