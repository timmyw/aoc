/*
 * day6b
 */

#include <vector>
#include <string>
#include <iostream>
#include <utility>
#include <cassert>
#include <memory>

#include "coord.h"
#include "strutils.h"
#include "io.h"

std::vector<std::string> lines;
int rows, cols;

int* visits = 0;

coord find_char(char c) {
  int y = 0;
  for (auto l : lines) {
    int x;
    if ((x = l.find(c)) != std::string::npos) {
      return coord{x, y};
    }
    y++;
  }
  return coord{-1, -1};
}

coord find_start() {
  auto c = find_char('^');
  if (c.r == -1)
    c = find_char('>');
  if (c.r == -1)
    c = find_char('v');
  if (c.r == -1)
    c = find_char('<');
  return c;
}

bool valid_coords(int r, int c) {
  return c >= 0 && c < cols && r >= 0 && r < rows;
}

bool valid_coords(coord& c) {
  return valid_coords(c.r, c.c);
}

char get_char(int r, int c) {
  if (!valid_coords(r,c))
    return '~';
  auto row = lines[r];
  return row[c];
}

char get_char(coord& c) {
  return get_char(c.r, c.c);
}

void set_char(int r, int c, char t) {
  if (!valid_coords(r, c))
    return;
  auto row = lines[r];
  row[c] = t;
  lines[r] = row;
}

void set_char(coord& c, char t) {
  set_char(c.r, c.c, t);
}

coord get_direction(char c) {
  switch (c) {
  case '^': return coord{0, -1};
  case '>': return coord{1, 0};
  case '<': return coord{-1, 0};
  case 'v': return coord{0, 1};
  }
  return coord{0,0};
}

char get_next_direction(char c) {
  switch (c) {
  case '^': return '>';
  case '>': return 'v';
  case 'v': return '<';
  case '<': return '^';
  }
  return '_';
}

void dump_board() {
  for (auto r : lines) {
    std::cout << r << std::endl;
  }
}

int count_x() {
  int cnt = 0;
  for (auto r : lines) {
    for (int i = 0; i < r.length(); i++)
      if (r[i] == 'X')
	cnt++;
  }
  return cnt;
}

void reset_visits() {
  for (int i = 0; i < rows; i++)
    for (int j = 0; j < cols; j++)
      visits[i*cols+j] = 0;
}

void inc_visit(coord& cur) {
  visits[cur.r * cols + cur.c]++;
}

int get_visit(coord& cur) {
  return visits[cur.r * cols + cur.c];
}

/*
  run through as normal, but don't set current positions with 'X'
  leave the direction marker in place
  at any point
  - if the new position contains the same char as the current direction, then we are looping.  I think
  Changes:
  we can't just search the board for the start char
 */
bool does_board_loop(int xb, int yb, int iter) {
  coord cur = find_start(), old;
  reset_visits();
  int cnt = 0;
  while (valid_coords(cur)) {
    cnt++;
    // if (cnt > 10003) {
    //   set_char(xb, yb, 'O');
    //   // dump_board();
    //   return false;
    // }
    // Store direction
    char c = get_char(cur);
    // std::cout << iter << ":(" << xb << "," << yb << ") cur:" << cur << "=" << c << std::endl;
    // Find direction and update position
    coord dir = get_direction(c);
    old = cur;
    cur.r += dir.r; cur.c += dir.c;
    // std::cout << "new: " << cur << get_char(cur) << std::endl;

    if (valid_coords(cur)) {
      char n = get_char(cur);
      if (n == '#') { 		// Obstacle, turn to the right
	cur = old;
	c = get_next_direction(c);
	// std::cout << "BUMP BACK:" << cur << ":" << c << std::endl;
      } else {
	if (n == c || get_visit(cur) > 10000) {		// We are looping
	  return true;
	}
      }
      // Place new start/direction
      set_char(cur, c);
      inc_visit(cur);
    } else
      return false;
  }
  return false; 		// We didn't loop
}

int main(int argc, char* argv[])
{
  lines = load_input(get_input_path() + "/6");
  rows = lines.size();
  cols = lines[0].length();

  visits = new int[(cols+1) * (rows+1)];

  int count = 0;
  int iter = 0;

  // set_char(5, 46, '#');
  // does_board_loop(5, 45, 1);
  // return 0;

  /*
    iterate through each position on the board that isn't an obstacle
    Place a new obstacle there and check if the board loops or if it still just exits
    Count up the loops
   */
  auto orig_lines = lines;	// Take a copy
  for (int i = 0; i < cols; i++) {
    for (int j = 0; j < rows; j++) {
      assert(valid_coords(i,j));
      std::cout << "Checking: (" << i << "," << j << ") [" << cols << "," << rows << "] ";
      if (get_char(i, j) == '.') {
	set_char(i, j, '#');
	if (does_board_loop(i, j, iter++)) {
	  std::cout << "LOOP";
	  count++;
	}
	lines = orig_lines;	// Awful way of doing things
      }
      std::cout << std::endl;
    }
  }

  std::cout << "Count: " << count << std::endl;

  return 0;
}

/*
  When the movement flips back 180 degrees on itself and then back again we can't
  track it as a loop.

  - When we are in direction X
  - Encounter block - so switch to direction X+1
  - Immediately encounter another block - so switch to X+2

  .....#...##.....^.^...........^.^......^.^...v..^..^...^^...
  ....#>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<#.^..^.#.^^...
  .##.....#.......^.^...........^.^...#..^.^...#..^..^...^^...

  Currently -
  If current char matches my current direction then we are in a loop.

  Maybe add:
  - if visited current pos (i.e. not '.')
  - current direction is opposite of current direction
    - and is > then check x-2 for a block and x-1,y-1 for a block
    - and so on for the other directions?
  - Have to check two positions 'back' as the rotation will have overwritten the original

  That won't work, as we don't know at that point if we are actually in a loop, or doubling back for the first time.

  Next thought
  - Add visit count to each position
  - If the visit count hits a really big number for the current pos, then assume a loop?
 */
