/*
 * day13a
 */

#include <iostream>
#include <regex>

#include "strutils.h"
#include "io.h"

std::pair<int, int> calculate_counts(int px, int ax, int ay, int py, int bx, int by) {
  int ca = (py - by*px/bx)/(ay-by*ax/bx);
  int cb = (px - ca*ax)/bx;
  std::cout << "A:" << ca << " B:" << cb << std::endl;
  return std::pair<int,int>{ca, cb};
}

bool calculate_solution(int ca, int cb, int px, int ax, int ay, int py, int bx, int by) {
  return (px == ca * ax + cb * bx && py == ca * ay + cb * by);
}

std::pair<int, int> iterate_solutions(int px, int ax, int ay, int py, int bx, int by) {
  for (int ca = 1; ca <= 100; ca++)
    for (int cb = 1; cb <= 100; cb++) {
      if (calculate_solution(ca, cb, px, ax, ay, py, bx, by))
	return std::pair<int, int>(ca, cb);
    }
  return std::pair<int, int>(0,0);
}

auto input = load_input(get_input_path() + "/test_13");

/*
Button A: X+94, Y+34
Button B: X+22, Y+67
Prize: X=8400, Y=5400
*/

std::pair<int, int> extract_xy(std::string s) {
  std::regex xy("[0-9]+");
  auto words_begin = std::sregex_iterator(s.begin(), s.end(), xy);
  auto words_end = std::sregex_iterator();

  std::string sx = (*words_begin).str();
  words_begin++;
  std::string sy = (*words_begin).str();
  return std::pair<int, int>(std::stoi(sx), std::stoi(sy));
}

int main(int argc, char* argv[])
{
  int cost = 0;
  for (int r = 0; r < input.size(); r++) {
    // Assume 3 lines per game + 1 blank
    std::string a = input[r++];
    auto xy = extract_xy(a);
    int ax = xy.first;
    int ay = xy.second;
    std::string b = input[r++];
    xy = extract_xy(b);
    int bx = xy.first;
    int by = xy.second;
    std::string prize = input[r];
    xy = extract_xy(prize);
    int px = xy.first;
    int py = xy.second;

    // auto c = calculate_counts(px, ax, ay, py, bx, by);
    auto c = iterate_solutions(px, ax, ay, py, bx, by);

    if (px == c.first * ax + c.second * bx &&
	py == c.first * ay + c.second * by) {
      std::cout << "solved: A=" << c.first << " B=" << c.second << std::endl;
      cost += 3 * c.first + c.second;
    }
    else
      std::cout << "not solved" << std::endl;
  }

  // auto c = calculate_counts(8400, 94, 34, 5400, 22, 67);
  // cost = 3 * c.first + c.second;
  std::cout << "Cost:" << cost << std::endl;
  return 0;
}

/*
 */
