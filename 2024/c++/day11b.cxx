/*
 * day11a
 */

#include <algorithm>
#include <string>
#include <iostream>
#include <vector>
#include <fstream>
#include <map>
#include <numeric>
#include <cmath>

#include <boost/format.hpp>

#include "io.h"
#include "strutils.h"

std::string inp = load_input_whole(get_input_path() + "/11");

typedef int64_t inttype;
typedef std::vector<std::string> string_v;
typedef std::map<inttype, inttype> pathmap;

template<typename T>
void dump_v(const T& v) {
  for (auto i : v) {
    std::cout << i << " ";
  }
  std::cout << std::endl;
}

void split_digits(inttype i, inttype& left, inttype& right) {
  const std::string s = str(boost::format("%ld") % i);
  auto l = s.size() / 2;
  left = std::stoll(s.substr(0, l));
  right = std::stoll(s.substr(l, s.size()-l));
}

std::vector<inttype> blink(inttype num) {
  if (num == 0) {
    return std::vector<inttype> {1};
  }

  inttype l = std::log10l(num) + 1;
  if (l % 2 == 0) {
    inttype left, right;
    split_digits(num, left, right);
    return std::vector<inttype> { left , right};
  }

  return std::vector<inttype> {num * 2024};
}

void dump_m(pathmap m) {
  for (auto i : m) {
    std::cout << " [" << i.first << "]=" << i.second;
  }
  std::cout << std::endl;
}

int main(int argc, char* argv[])
{
  pathmap mm;
  string_v stones;
  split_string(inp, stones, ' ');
  for (auto i : stones)
    mm[std::stoll(i)] = 1;
  dump_m(mm);

  int iter_max = 75;
  for (int i = 1; i <= iter_max; i++) {
    std::cout << "Iteration " << i << "..." << std::endl;
    pathmap tmp;
    for (auto j : mm) {
      for (int i = 0; i < j.second; i++) {
	std::vector<inttype> k = blink(j.first); // Get the replacement number(s).
	// std::cout << j.first << " => ";
	// dump_v(k);
	for (auto num : k) {
	  if (tmp.find(num) != tmp.end())
	    tmp[num] += 1;
	  else
	    tmp[num] = 1;
	}
      }
    }
    mm = tmp;
    // std::cout << "ITER:" << i << " -->";
    // dump_m(mm);
  }

  inttype count = std::accumulate(mm.begin(), mm.end(), 0, [](int value, const pathmap::value_type& p) {
    // std::cout << p.first << ":" << p.second << std::endl;
    return value + p.second;
  });
  std::cout << "length:" << count << std::endl;
  return 0;
}
