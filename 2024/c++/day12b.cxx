/*
 * day12b
 */

#include <vector>
#include <string>
#include <iostream>
#include <utility>
#include <boost/format.hpp>

#include "strutils.h"
#include "board.h"

auto input = load_board(get_input_path() + "/12");

void build_working(board &main, board& working, int row, int col, char plant) {
  if (!main.valid_coords(row, col))
    return;
  if (main.get_char(row, col) == plant) {
    working.set_char(row, col, plant);
    main.set_char(row, col, tolower(plant));
    build_working(main, working, row-1, col, plant);
    build_working(main, working, row, col+1, plant);
    build_working(main, working, row+1, col, plant);
    build_working(main, working, row, col-1, plant);
  }
}

coord
change_dir(const coord& dir) {
  if (dir.r == 0 && dir.c == 1)
    return coord{1, 0};
  if (dir.r == 1 && dir.c == 0)
    return coord{0, -1};
  if (dir.r == 0 && dir.c == -1)
    return coord{-1, 0};
  if (dir.r == -1 && dir.c == 0)
    return coord{0, 1};
  return coord{0,0};
}

/*
  > -> ^
  v -> >
  < -> v
  ^ -> <
 */
coord
change_orth_dir(const coord& dir) {
  if (dir == dir_right)
    return dir_up;
  if (dir == dir_down)
    return dir_right;
  if (dir == dir_left)
    return dir_down;
  if (dir == dir_up)
    return dir_left;
  return coord{0,0};
}

bool empty(char ch) {
  return (ch == '~' || ch == 0) ? true : false;
}

/*
  number of sides

  - Find first 'horizontal' edge - row by row
  - count to the right.  if edge breaks, then inc count and keep going
  - do the next row
 */
int count_sides(const board& b, char plant) {
  int count = 0;
  for (int r = 0; r <= b.rows; r++) {
    // Count horizontal 'top' edges
    for (int c = 0; c <= b.cols; c++) {
      char ch = b.get_char(r, c);
      if (ch == plant && empty(b.get_char(r-1,c))) {
	auto start = c;
	count++;
	while (c <= b.cols && b.get_char(r, c) == plant && empty(b.get_char(r-1,c)))
	  c++;
	// std::cout << "horiz top from " << start << " to " << c-1 << std::endl;
      }
    }
    // Count horizontal 'bottom' edges
    for (int c = 0; c <= b.cols; c++) {
      char ch = b.get_char(r, c);
      if (ch == plant && empty(b.get_char(r+1,c))) {
	auto start = c;
	count++;
	while (c <= b.cols && b.get_char(r, c) == plant && empty(b.get_char(r+1,c)))
	  c++;
	// std::cout << "horiz bottom from " << start << " to " << c-1 << std::endl;
      }
    }
  }

  for (int c = 0; c <= b.cols; c++) {
    // Count vertical 'left' edges
    std::cout << "col:" << c << std::endl;
    for (int r = 0; r <= b.rows; r++) {
      char ch = b.get_char(r, c);
      if (ch == plant && empty(b.get_char(r,c-1))) {
	auto start = r;
	count++;
	while (r <= b.rows && b.get_char(r, c) == plant && empty(b.get_char(r,c-1)))
	  r++;
	std::cout << "vert left from " << start << " to " << r-1 << std::endl;
      }
    }
    // Count vertical 'right' edges
    for (int r = 0; r <= b.rows; r++) {
      char ch = b.get_char(r, c);
      if (ch == plant && empty(b.get_char(r,c+1))) {
	auto start = r;
	count++;
	while (r <= b.rows && b.get_char(r, c) == plant && empty(b.get_char(r,c+1)))
	  r++;
	std::cout << "vert right from " << start << " to " << r-1 << std::endl;
      }
    }
  }

  return count;
}

int main(int argc, char* argv[])
{
  int cost = 0;
  for (int r = 0; r < input.rows; r++)
    for (int c = 0; c < input.cols; c++) {
      char plant = input.get_char(r, c);
      if (std::islower(plant))	// We have already covered this
	continue;
      board working(input.rows, input.cols, 0);
      build_working(input, working, r, c, plant);
      input.dump(working);

      // Count the number of plants in working
      int count = working.count(plant);

      // Count the perimeter
      int perimeter = count_sides(working, plant);
      std::cout << "price: " << count << " * " << perimeter << " = " << count*perimeter << std::endl;
      std::cout << std::endl;

      cost += count * perimeter;
    }

  std::cout << "Cost:" << cost << std::endl;
  return 0;
}

/*
  Work through board
  For each untouched plant
  - create a working board
  - walk through the area for current plant - marking on the working board, and marking upper case on the main board.

    - Fan out recursively - use same boards.  If recursive call finds no same plants
      or already marked ones it returns

  - trace all edges of the working board (how?) to calculate perimeter and area
  -

  RRRR......
  RRRR......
  ..RRR.....
  ..R.......
  ..........
  ..........
  ..........
  ..........

  - Find first (top left?)
  - Set current direction/facing - down
  - Walk left until we hit a boundary or blank (non plant)
  -

   (left, forwards, right, backwards)

  .....
  .AA..
  ..AA.
  AAA..
  .....

 */
