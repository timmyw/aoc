(* Day 1 *)

open Core

let input = "../input/1"

let create_lists lines =
  List.fold ~init:([],[]) ~f:(fun (l,r) line ->
      let nums = String.split ~on:' ' line in
      (* let _ = printf "[%s] [%s]\n" (Twutils.Options.strip_option "" (List.nth nums 0)) (Twutils.Options.strip_option "" (List.nth nums 3)) in *)
      (Int.of_string (Twutils.Options.strip_option "" (List.nth nums 0)) :: l,
       Int.of_string (Twutils.Options.strip_option "" (List.nth nums 3)) :: r)) lines

let process_lists l r =
  let rec process_lists' acc l r =
    match (l,r) with
    | ([], []) -> acc
    | (x::xs, y::ys) ->
      let _ = printf "%d-%d=%d\n" x y (abs (x-y)) in
      process_lists' (acc + abs (x - y)) xs ys
    | _ -> acc
  in
  process_lists' 0 l r

let rec dump_list l =
  match l with
  | [] -> let _ = print_endline "" in ()
  | x::xs -> let _ = printf "%d " x in dump_list xs

let () =
  let lines = In_channel.read_lines input in
  let (l', r') = create_lists lines in
  let (l, r) = (List.sort ~compare:compare l',
                List.sort ~compare:compare r') in
  let _ = dump_list l in
  let _ = dump_list r in
  let m = process_lists l r in
  printf "%d\n" m
