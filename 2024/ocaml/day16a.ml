(* Day16a *)

open Aocutils
(* open Core *)

let input = "../input/test_16"

let () =
  let _ = print_endline "16a" in
  let b = Board.load_file input in
  Board.dump b
