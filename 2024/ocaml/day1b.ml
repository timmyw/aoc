(* Day 1 *)

open Core
open Aocutils

let input = "../input/1"

let create_lists lines =
  List.fold ~init:([],[]) ~f:(fun (l,r) line ->
      let nums = String.split ~on:' ' line in
      (* let _ = printf "[%s] [%s]\n" (Twutils.Options.strip_option "" (List.nth nums 0)) (Twutils.Options.strip_option "" (List.nth nums 3)) in *)
      (Int.of_string (Twutils.Options.strip_option "" (List.nth nums 0)) :: l,
       Int.of_string (Twutils.Options.strip_option "" (List.nth nums 3)) :: r)) lines

let process_lists l r =
  List.fold ~init:0
    ~f:(fun acc i ->
        acc + i * Lists.find_count r i)
    l

let () =
  let lines = In_channel.read_lines input in
  let (l, r) = create_lists lines in
  let m = process_lists l r in
  printf "%d\n" m
